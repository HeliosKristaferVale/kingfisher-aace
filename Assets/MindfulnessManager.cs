﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MindfulnessManager : MonoBehaviour
{
    private Transform[] children;

    [SerializeField]
    private GameObject[] interactiveKeys;
    [SerializeField]
    private GameObject playbackMesh;
    [SerializeField]
    private float rotationOffset;
    [SerializeField]
    private float introVODuration;
    [SerializeField]
    private MeshRenderer transitionSphere;
    private bool isOpen = false;

    void Awake()
    {
        children = GetComponentsInChildren<Transform>();
        for (int i = 0; i < children.Length; i++)
        {
            if (i != 0)
            {
                children[i].gameObject.SetActive(false);
            }
        }

        transitionSphere.material.DOFade(0f, 0f);
    }


    public void LaunchMindfulness(float delay)
    {
        isOpen = true;
        playbackMesh.transform.DORotate(new Vector3(0, rotationOffset, 0), 0f);
        StartCoroutine(LaunchMindfulnessDelayed(delay, introVODuration));
    }

    public void ExitMindfulness(float delay)
    {
        isOpen = false;
        StartCoroutine(ExitMindfulnessDelayed(delay));
    }

    private IEnumerator LaunchMindfulnessDelayed(float delay, float introVODuration)
    {
        yield return new WaitForSeconds(delay);

        if (!isOpen)
        {
            yield break;
        }

        transitionSphere.material.DOFade(.75f, 0f);

        yield return new WaitForSeconds(introVODuration);

        if (!isOpen)
        {
            yield break;
        }

        transitionSphere.material.DOFade(0, 2f);

        for (int i = 0; i < children.Length; i++)
        {
            children[i].gameObject.SetActive(true);
        }

        foreach (GameObject interactiveKey in interactiveKeys)
        {
            KeyFadeInSequence(interactiveKey.transform);
        }
    }

    private IEnumerator ExitMindfulnessDelayed(float delay)
    {
        foreach (GameObject interactiveKey in interactiveKeys)
        {
            KeyFadeOutSequence(interactiveKey.transform);
        }

        yield return new WaitForSeconds(delay);
        transitionSphere.material.DOFade(0f, 2f);

        for (int i = 0; i < children.Length; i++)
        {
            if (i != 0)
            {
                children[i].gameObject.SetActive(false);
            }
        }

        playbackMesh.transform.DORotate(new Vector3(0, 0, 0), 0f);
    }

    private void KeyFadeInSequence(Transform keyParent)
    {
        keyParent.DOMoveY(keyParent.position.y + 1f, 2f)
        .SetEase(Ease.OutBack);

        CanvasGroup cg = keyParent.GetComponent<CanvasGroup>();

        if (cg == null)
        {
            cg = keyParent.GetComponentInChildren<CanvasGroup>();
        }

        cg.DOFade(1, 1f);
    }

    private void KeyFadeOutSequence(Transform keyParent)
    {
        keyParent.DOMoveY(keyParent.position.y + -1f, 2f)
        .SetEase(Ease.InBack);

        CanvasGroup cg = keyParent.GetComponent<CanvasGroup>();

        if (cg == null)
        {
            cg = keyParent.GetComponentInChildren<CanvasGroup>();
        }

        cg.DOFade(0, 1f);
    }


}
