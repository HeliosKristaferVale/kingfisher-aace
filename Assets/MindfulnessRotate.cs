﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;

    public class MindfulnessRotate : MonoBehaviour
    {
        public Transform Content;
        public bool FollowHorizontally;
        public bool FollowVertically;
        public float TetherFoV;

        private void Update()
        {
            Transform camTransform = Camera.main.transform;
            Vector3 dirToContent = (Content.position - camTransform.position).normalized;
            Vector3 contentAbsRot = Quaternion.LookRotation(dirToContent, Vector3.up).eulerAngles;

            if(FollowHorizontally)
            {
                float offset = Mathf.DeltaAngle(camTransform.eulerAngles.y, contentAbsRot.y);
                if(Mathf.Abs(offset) > TetherFoV)
                {
                    float counterRot = (Mathf.Abs(offset) - TetherFoV) * -Mathf.Sign(offset);
                    Content.RotateAround(camTransform.position, Vector3.up, counterRot);
                }
            }
            if (FollowVertically)
            {
                float offset = Mathf.DeltaAngle(camTransform.eulerAngles.x, contentAbsRot.x);
                if (Mathf.Abs(offset) > TetherFoV)
                {
                    float counterRot = (Mathf.Abs(offset) - TetherFoV) * -Mathf.Sign(offset);
                    Content.RotateAround(camTransform.position, camTransform.right, counterRot);
                }
            }
        }

        // public override void LoadFromAsset(BaseAsset asset)
        // {
        //     // Need to get the FollowView asset out which requires us to spawn whatever asset it wraps, no recusion supported. 
        //     FollowViewAsset viewAsset = asset as FollowViewAsset;
        //     if(viewAsset == null)
        //         throw new ArgumentException("asset is expected to be of type FollowViewAsset");

        //     BaseAsset childAsset = viewAsset.FollowingAsset;
        //     if (childAsset == null)
        //         throw new ArgumentException("FollowingAsset is null, don't have nothing follow your view!");

        //     // Instantiate our asset and let it load itself from the asset
        //     UIBasePrefab childPrefab = Instantiate(childAsset.Prefab);
        //     childPrefab.transform.SetParent(transform);
        //     childPrefab.LoadFromAsset(childAsset);

        //     // Now that we've loaded our from asset, it should do any positioning logic it desires.
        //     // That effectively determines our start x,y so that if you only follow view on one direction
        //     // then it's at the correct location. We assume the prefab is the object that is supposed to follow...
        //     m_content = childPrefab.transform;

        //     m_tetherFoV = viewAsset.MaximumFieldOfView;
        //     FollowHorizontally = viewAsset.FollowHorizontally;
        //     FollowVertically = viewAsset.FollowVertically;
        // }
    }
