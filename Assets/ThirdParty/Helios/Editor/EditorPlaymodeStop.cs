﻿using UnityEditor;
using UnityEngine;

namespace Kingfisher.Editor
{
    [InitializeOnLoad]
    public class EditorEvents
    {
        // InitializeOnLoad causes this to get called when the editor starts up
        static EditorEvents()
        {
            EditorApplication.update += Update;
        }

        static void Update()
        {
            // If compiling starts while in play mode, it's often really useful to just quit now before the errors start spinning up!
            if (EditorApplication.isPlaying && EditorApplication.isCompiling)
                EditorApplication.isPlaying = false;
        }
    }
}