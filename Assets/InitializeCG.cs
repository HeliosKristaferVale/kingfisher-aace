﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeCG : MonoBehaviour {

    private CanvasGroup myCG;
    public bool isActiveByDefault = false;  

    void Awake()
    {
        myCG = GetComponent<CanvasGroup>();

        if (isActiveByDefault)
        {
            myCG.alpha = 1f;
            myCG.interactable = true;
            myCG.interactable = true;
        }
        else
        {
            myCG.alpha = 0f;
            // myCG.interactable = false;
            // myCG.blocksRaycasts = false;
        }
       
    }
}
