﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TransitionManager : MonoBehaviour
{

    [Header("Elements")]
    public GameObject[] CenterElements;
    public GameObject[] LeftElements;
    public GameObject[] RightElements;

    [Header("Audio")]
    public AudioClip vOAudioClip;

    [Header("Color")]
    public Color InOutColor;
    public Color OcclusionColor;

    private AudioSource vOAudioSource;
    private MeshRenderer sphereMesh;

    void Awake()
    {
        vOAudioSource = gameObject.AddComponent<AudioSource>();
        vOAudioSource.loop = false;

        sphereMesh = GetComponent<MeshRenderer>();

        foreach (GameObject element in CenterElements)
        {
            element.SetActive(false);
        }

        foreach (GameObject element in LeftElements)
        {
            element.SetActive(false);
        }

        foreach (GameObject element in RightElements)
        {
            element.SetActive(false);
        }
    }

    void Start()
    {
        vOAudioSource.PlayOneShot(vOAudioClip);
        sphereMesh.material.DOColor(InOutColor, 0f); //Set sphere to primary color immediately

        sphereMesh.material.DOColor(OcclusionColor, 5f) //Fade sphere to occlusion color after a short delay
        .SetDelay(1f)
        .SetId("occlusionSphere");

        sphereMesh.material.DOFade(0f, 2f) //Fade sphere to invisible after the VO has finished
        .SetDelay(vOAudioClip.length)
        .OnComplete(() =>
        {
            foreach (GameObject element in CenterElements)
            {
                element.SetActive(true);
            }

            foreach (GameObject element in LeftElements)
            {
                element.SetActive(true);
            }

            foreach (GameObject element in RightElements)
            {
                element.SetActive(true);
            }

            KeyFadeInSequence(CenterElements[1].transform);
            KeyFadeInSequence(LeftElements[1].transform);
            KeyFadeInSequence(RightElements[1].transform);
        });
    }

    private void KeyFadeInSequence(Transform keyParent)
    {
        // keyParent.DOMoveY(keyParent.position.y - 1f, 0f);
        keyParent.DOMoveY(keyParent.position.y + 1f, 2f)
        .SetEase(Ease.OutBack);

        CanvasGroup cg = keyParent.GetComponent<CanvasGroup>();

        if (cg == null)
        {
            cg = keyParent.GetComponentInChildren<CanvasGroup>();
        }

        cg.DOFade(1, 1f);
    }

    private void KeyFadeOutSequence(Transform keyParent)
    {
        // keyParent.DOMoveY(keyParent.position.y - 1f, 0f);
        keyParent.DOMoveY(keyParent.position.y + -1f, 2f)
        .SetEase(Ease.InBack);

        CanvasGroup cg = keyParent.GetComponent<CanvasGroup>();

        if (cg == null)
        {
            cg = keyParent.GetComponentInChildren<CanvasGroup>();
        }

        cg.DOFade(0, 1f);
    }

    public void OnHomeButtonClicked() //Assigned on button callback in inspector
    {
        KeyFadeOutSequence(CenterElements[1].transform);
        KeyFadeOutSequence(LeftElements[1].transform);
        KeyFadeOutSequence(RightElements[1].transform);

        DOTween.Kill("occlusionSphere");
        sphereMesh.material.DOColor(InOutColor, 2f) //Fade sphere to occlusion color after a short delay
            .SetDelay(1f)
            .OnComplete(() =>
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("main_application");
            });
    }
}

