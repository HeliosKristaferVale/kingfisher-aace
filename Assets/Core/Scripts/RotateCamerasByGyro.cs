﻿using UnityEngine;
using UnityEngine.VR;

public class RotateCamerasByGyro : MonoBehaviour
{
    public void SetVRModeEnabled(bool enabled)
    {
        bool wasEnabled = UnityEngine.XR.XRSettings.enabled;
        UnityEngine.XR.XRSettings.enabled = enabled;
        if (enabled && !wasEnabled)
        {
            UpdateHeadTrackingForVRCameras();
        }
    }

    private void Update()
    {
        if (!UnityEngine.XR.XRSettings.enabled)
        {
            UpdateHeadTrackingForVRCameras();
        }
    }

    private void UpdateHeadTrackingForVRCameras()
    {
        Quaternion pose = UnityEngine.XR.InputTracking.GetLocalRotation(UnityEngine.XR.XRSettings.enabled ? UnityEngine.XR.XRNode.Head : UnityEngine.XR.XRNode.CenterEye);
        Camera[] cams = Camera.allCameras;
        for (int i = 0; i < cams.Length; i++)
        {
            Camera cam = cams[i];
            if (cam.targetTexture == null && cam.cullingMask != 0)
            {
                cam.GetComponent<Transform>().localRotation = pose;
            }
        }
    }
}
