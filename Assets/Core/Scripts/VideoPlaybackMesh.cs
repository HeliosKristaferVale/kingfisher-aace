﻿using UnityEngine;
using DG.Tweening;

namespace Kingfisher
{
    /// <summary>
    /// Which projection format is our video format in? If you add a <see cref="ProjectionFormats"/>
    /// then you need to add a matching Mesh to the m_projectionMeshes array.
    /// </summary>
    public enum ProjectionFormats
    {
        Equirectangular,
    }

    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class VideoPlaybackMesh : MonoBehaviour
    {
        [SerializeField] private Mesh[] m_projectionMeshes;
        private Color m_lightboxColor = new Color(0.8f, 0.8f, 0.8f, 1f);
        public ProjectionFormats ProjectionFormat { get; protected set; }

        private MeshFilter m_meshFilter;
        private Material m_materialInstance;

        private void Awake()
        {
            SetProjectionFormat(ProjectionFormats.Equirectangular);

            if (m_materialInstance == null)
                m_materialInstance = GetComponent<MeshRenderer>().material;
        }

        private void OnDestroy()
        {
            Destroy(m_materialInstance);
        }

        public void SetProjectionFormat(ProjectionFormats format)
        {
            if ((int)format < 0 || (int)format >= m_projectionMeshes.Length)
            {
                Debug.LogWarningFormat("Attempted to set unknown format ({0}) or out of bounds index ({1}), not setting Projection Format!", format, (int)format);
                return;
            }

            if (m_meshFilter == null)
                m_meshFilter = GetComponent<MeshFilter>();

            m_meshFilter.sharedMesh = m_projectionMeshes[(int)format];
        }

        public void SetPlaybackTexture(Texture texture, bool requiresYFlip)
        {
            // Ensure it's not null here. We double check in case execution order of Awake functions means that
            // this one isn't awake before the other bits.
            if (m_materialInstance == null)
                m_materialInstance = GetComponent<MeshRenderer>().material;

            if (requiresYFlip)
            {
                m_materialInstance.mainTextureScale = new Vector2(1f, -1f);
            }
            else
            {
                m_materialInstance.mainTextureScale = new Vector2(1f, 1f);
            }

            m_materialInstance.SetTexture("_MainTex", texture);
        }

        public void SetLightboxMode(bool bEnabled)
        {
            Color endColor = bEnabled ? m_lightboxColor : Color.white;
            m_materialInstance.DOKill();
            m_materialInstance.DOColor(endColor, 2f).SetSpeedBased().SetEase(Ease.InOutCirc);
        }
    }
}