﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kingfisher;
using RenderHeads.Media.AVProVideo;
using AVProMediaPlayer = RenderHeads.Media.AVProVideo.MediaPlayer;
using UnityEngine;

namespace Kingfisher
{
    public sealed class KingfisherAvProMediaPlayer : KingfisherMediaPlayer
    {
        /// <summary> Called when the MediaPlayer has loaded a video and created a texture.
        /// Will be called again when the MediaPlayer is destroyed with a null value for Texture.
        /// It is important that you stop using the originally passed texture as it crashes AVPro!
        /// The bool specifies if a Y flip is needed, please do this in your shader/display.
        /// </summary>
        private AVProMediaPlayer m_mediaPlayerComponent;         // This needs to be made implementation agnostic.

        private void Awake()
        {
            m_mediaPlayerComponent = gameObject.AddComponent<AVProMediaPlayer>();
            m_mediaPlayerComponent.Events.AddListener(OnAVProMediaEvent);
            m_mediaPlayerComponent.m_AutoOpen = false;
            m_mediaPlayerComponent.m_AutoStart = false;
            m_mediaPlayerComponent.m_WrapMode = TextureWrapMode.Repeat;
            m_mediaPlayerComponent.PlatformOptionsAndroid.useFastOesPath = false; // Leaving this one for the next time we need more perf. See below.
            m_mediaPlayerComponent.PlatformOptionsAndroid.showPosterFrame = true; // Show first frame when loaded before playing.

            // The OES path is not enabled by default because it requires some special care to be taken
            // and can be tricky for beginners. When this option is enabled the Android GPU returns
            // special OES textures (see EGL extension OES_EGL_image_external) that are hardware
            // specific. Unfortunately Unity isn’t able to use these textures directly, so you can’t just map
            // them to a material or UI. To use the texture a GLSL shader must be used. Unfortunately
            // Unity’s GLSL support isn’t as good as its CG shader support so again this makes things
            // more tricky. The GLSL compiler only happens on the device (not inside Unity) so errors in
            // the shader can be difficult to debug.
        }

        // Both
        private void Update()
        {
            if (!m_isPlaying)
            {
                return;
            }

            if (m_mediaPlayerComponent.Control != null)
            {
                CurrentTimeInSeconds = m_mediaPlayerComponent.Control.GetCurrentTimeMs() / 1000f;
            }
        }

        // Both
        public override void LoadMediaAsync(string mediaName, Action onFinishedLoading)
        {
            m_isPlaying = false;
            m_mediaIsLoaded = false;
            CurrentTimeInSeconds = 0f;
            DurationInSeconds = 0f;
            m_onFinishedLoadingCallback = onFinishedLoading;

            // This will automatically close any existing file that is playing.
            string filePath = m_application.DoPathFixup(mediaName);

            if (m_mediaPlayerComponent.VideoOpened)
            {
                Debug.LogWarningFormat("KingfisherMediaPlayer:LoadMedia called while a video was still open. This may lead to issues? Current Media: {0}", m_mediaPlayerComponent.m_VideoPath);
            }

            m_mediaPlayerComponent.OpenVideoFromFile(AVProMediaPlayer.FileLocation.AbsolutePathOrURL, filePath, false);
        }

        public override void UnloadMedia()
        {
            m_mediaPlayerComponent.CloseVideo();
        }

        public override void SetLoops(bool loops)
        {
            m_mediaPlayerComponent.m_Loop = loops;
        }

        public override void Play()
        {
            if (!m_mediaIsLoaded)
            {
                Debug.LogWarning("Media not loaded yet, ignoring call to play.");
                return;
            }

            m_mediaPlayerComponent.Play();
        }

        public override void Pause()
        {
            if (!m_mediaIsLoaded)
            {
                Debug.LogWarning("Media not loaded yet, ignoring call to pause.");
                return;
            }

            m_mediaPlayerComponent.Pause();
        }

        public override void Stop()
        {
            DurationInSeconds = 0f;
            CurrentTimeInSeconds = 0f;
            m_isPlaying = false;

            m_mediaPlayerComponent.Stop();
        }

        private void OnAVProMediaEvent(AVProMediaPlayer mediaPlayerComponent, MediaPlayerEvent.EventType eventType, ErrorCode error)
        {
            Debug.LogFormat("AVProEvent: {0}", eventType);

            switch (eventType)
            {
                case MediaPlayerEvent.EventType.ReadyToPlay:
                    //m_mediaIsLoaded = true;
                    //if (m_onFinishedLoadingCallback != null)
                    //    m_onFinishedLoadingCallback.Invoke();
                    break;
                case MediaPlayerEvent.EventType.Started:
                    m_isPlaying = true;
                    break;
                case MediaPlayerEvent.EventType.MetaDataReady:
                    if (m_mediaPlayerComponent.Info != null)
                        DurationInSeconds = m_mediaPlayerComponent.Info.GetDurationMs() / 1000f;
                    break;

                case MediaPlayerEvent.EventType.FirstFrameReady:
                    if (OnPlaybackTextureChanged != null && m_mediaPlayerComponent.TextureProducer != null)
                        OnPlaybackTextureChanged.Invoke(m_mediaPlayerComponent.TextureProducer.GetTexture(), m_mediaPlayerComponent.TextureProducer.RequiresVerticalFlip());

                    StartCoroutine(DelayOneFrameAfterReadyForStart());
                    break;

                case MediaPlayerEvent.EventType.Closing:
                    m_mediaIsLoaded = false;
                    DurationInSeconds = 0f;
                    CurrentTimeInSeconds = 0f;

                    // AVPro Video seems to crash if we're still using the texture when the video is unloaded.
                    if (OnPlaybackTextureChanged != null)
                        OnPlaybackTextureChanged.Invoke(null, false);

                    break;
                case MediaPlayerEvent.EventType.FinishedPlaying:
                    if (OnVideoPlaybackFinished != null)
                        OnVideoPlaybackFinished.Invoke();
                    break;

                case MediaPlayerEvent.EventType.Error:
                    Debug.LogErrorFormat("AVPro Error: {0}", error);
                    break;
            }
        }

        private IEnumerator DelayOneFrameAfterReadyForStart()
        {
            // We seem to have to wait one frame after the media texture is ready
            // before it's actually not white?
            yield return null;

            m_mediaIsLoaded = true;
            if (m_onFinishedLoadingCallback != null)
                m_onFinishedLoadingCallback.Invoke();
        }
    }
}
