﻿using UnityEngine;

namespace Kingfisher
{
    public class BaseStateComponent : MonoBehaviour
    {
        protected FiniteStateMachine m_parentMachine;

        internal virtual void SetStateMachine(FiniteStateMachine machine)
        {
            m_parentMachine = machine;
        }
    }
}