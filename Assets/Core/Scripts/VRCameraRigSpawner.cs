﻿using UnityEngine;
using UnityEngine.XR;

namespace Kingfisher
{
    public class VRCameraRigSpawner : MonoBehaviour
    {
        public Camera UserCamera { get { return m_camera; } }
        public GameObject CameraManager { get { return m_spawnedManager; } }

        // This allows us to rotate the camera rigs per-scene 
        [SerializeField] private Transform m_cameraOffsetTransform;

        [Header("Cardboard")]
        [SerializeField] private Transform m_cardboardCameraRig;
        [SerializeField] private GameObject m_cardboardManager;

        [Header("GearVR")]
        [SerializeField] private Transform m_gearvrCameraRig;

        [Header("Daydream")]
        [SerializeField] private Transform m_daydreamCameraRig;
        [SerializeField] private GameObject m_daydreamManager;


        private Transform m_spawnedCameraRig;
        private Camera m_camera;
#pragma warning disable 0649 // Never assigned to, always null emits if editor not set to Android/Daydream.
        private GameObject m_spawnedManager;
#pragma warning restore 0414

        private void Awake()
        {
            Debug.Log("VRCameraRigSpawner::Is Awake.");
#if GOOGLE_CARDBOARD && !UNITY_EDITOR
        if(m_cardboardManager != null)
            m_spawnedManager = Instantiate(m_cardboardManager, m_cameraOffsetTransform, false);

        m_spawnedCameraRig = Instantiate(m_cardboardCameraRig, m_cameraOffsetTransform, false);
        m_camera = m_spawnedCameraRig.Find("Camera").GetComponent<Camera>();
        Debug.Log("Trying to spawn GoogleCardboard CameraRig.");
#elif GEAR_VR && !UNITY_EDITOR
        m_spawnedCameraRig = Instantiate(m_gearvrCameraRig);
        m_camera = m_spawnedCameraRig.GetComponent<Camera>();
        Debug.Log("Trying to spawn GearVR CameraRig.");
#elif GOOGLE_DAYDREAM
            m_spawnedCameraRig = Instantiate(m_daydreamCameraRig, m_cameraOffsetTransform, false);
            m_camera = m_spawnedCameraRig.Find("MainCamera").GetComponent<Camera>();
            if (m_daydreamManager != null)
                m_spawnedManager = Instantiate(m_daydreamManager, m_cameraOffsetTransform, false);
            Debug.Log("Trying to spawn Google Pixle CameraRig.");
#else
            // We use the GearVR Camera Rig as the fallback since it's got in-editor debug controls
            // and isn't actually any different than a MonoCamera when Unity isn't in VR mode.
            m_spawnedCameraRig = Instantiate(m_gearvrCameraRig, m_cameraOffsetTransform, false);
            m_camera = m_spawnedCameraRig.GetComponent<Camera>();
            Debug.Log("Trying to spawn Generic Camera Rig.");
#endif
        }

        public void RecenterView()
        {
#if GOOGLE_CARDBOARD && !UNITY_EDITOR
            if(m_spawnedManager != null)
            {
                GvrViewer cardboardViewer = m_spawnedManager.GetComponent<GvrViewer>();
                cardboardViewer.Recenter();
            }
#elif GEAR_VR
            // For some reason the Manager is on the Camera prefab for this one...
            //OVRManager.display.RecenterPose(); // This has been deprecated.
#endif
        }
    }
}