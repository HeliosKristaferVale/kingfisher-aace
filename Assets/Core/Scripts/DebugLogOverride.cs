﻿// If you're going to record a video and the performance is choppy in editor due
// to logs of debug logging (the media player is guilty of this) then you can
// define the following symbol to prevent the logs from being generated. This
// fixes performance issues.
// #define NO_LOG_IN_EDITOR

using UnityEngine;

namespace Kingfisher
{
    public class DebugLogOverride
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Init()
        {
#if UNITY_2017_1_OR_NEWER
            // Due to the high volume of printing, we surpress logs on non-debug builds.
    #if !DEBUG || NO_LOG_IN_EDITOR
            UnityEngine.Debug.unityLogger.logEnabled = false;
    #endif
#endif
        }
    }
}