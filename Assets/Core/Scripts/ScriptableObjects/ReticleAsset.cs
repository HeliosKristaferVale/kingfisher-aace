﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Kingfisher/Misc/Default Crosshair")]
    public class ReticleAsset : ScriptableObject
    {
        public Reticle Prefab;
        public float Distance = 5f;
        public float Scale = 1f;
        public Texture Sprite;
    }
}

