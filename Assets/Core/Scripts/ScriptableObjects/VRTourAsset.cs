﻿using Kingfisher.ScriptableObjects.Scenes;
using UnityEngine;

namespace Kingfisher.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Kingfisher/VR Tour")]
    public class VRTourAsset : ScriptableObject
    {
        public string ExperienceName = "Unnamed Experience";
        public BaseSceneAsset DefaultState;

        /// <summary>
        /// This is the name of the folder placed on the SD card (such as 'DemoTour', resulting in a path of
        /// /sdcard/DemoTour) in which all of the related video assets and files will go. This folder exists to
        /// make organization easy, but also allows the application to ensure the required files have been installed.
        /// </summary>
        [Tooltip("Name of the folder placed on the root of SDCard which holds all tour-related files (videos)")]
        public string SDCardRootFolder = "DemoTour";

        public ReticleAsset Reticle;
        public Color TransitionColor = new Color(0.04f, 0.27f, 0.55f, 1f);
        public ProjectionFormats VideoProjectionFormat = ProjectionFormats.Equirectangular;
        public BaseSceneAsset[] VRTourSceneList;
    }
}