﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Video (Static)")]
    public class StaticVideoAsset : BaseTransformedAsset
    {
        /// <summary> Settings to use on the video player for this Popup. </summary>
        [Tooltip("Settings to use on the video player for this Popup.")]
        public BaseVideoAsset VideoSettings;

        /// <summary> Settings to use on the background audio for this Popup. </summary>
        [Tooltip("Settings to use on the background audio for this Popup.")]
        public BaseAudioDuckingAsset DuckingSettings;
    }
}