﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Image (Popup)")]
    public class PopupImageAsset : BasePopupAsset
    {
        public Sprite Image;
    }
}