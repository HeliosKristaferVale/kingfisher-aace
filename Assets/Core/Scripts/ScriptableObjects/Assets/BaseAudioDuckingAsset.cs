﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects
{
    /// <summary>
    /// There are many different places that background audio can be ducked. This struct contains
    /// common functionality for *all* objects that can cause audio ducking. Specific implementations
    /// (such as Popups) may add additional functionality and thus further settings in their own uses.
    /// </summary>
    [System.Serializable]
    public class BaseAudioDuckingAsset
    {
        /// <summary> If true we should mute any <see cref="UISceneAudio"/> (VO) clips. This lets you cut off scene VO if a higher priority audio source starts. </summary>
        [Tooltip("If true we should mute any UISceneAudio (VO) clips. This lets you cut off scene VO if a higher priority audio source starts.")]
        public bool MuteSceneAudio = true;

        /// <summary> If true, automatically ducks the background music when opened. </summary>
        [Tooltip("If true, automatically ducks the background music when opened.")]
        public bool DuckBackgroundAudio = true;

        /// <summary> How many db to duck the background audio? </summary>
        [Tooltip("How many db to duck the background audio?")]
        public float DuckAmountInDb = -12f;

        /// <summary> 
        /// If true, automatically unducks the background music when closed. This can be used
        /// in situations where the audio source is played at the end of the scene and a new
        /// audio source is played at the start of the next scene - it unducks and then ducks
        /// immediately across the scene transition and it's awkward. Solution: Don't unduck
        /// the audio at the end using this option. Only works in special cases, but the 
        /// unduck/reduck problem is a special case anyways!
        /// </summary>
        [Tooltip("If true, automatically unducks the background music when closed.")]
        public bool UnduckBackgroundAudio = true;
    }
}