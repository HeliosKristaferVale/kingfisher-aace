﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Video Background Controller")]
    public class VideoBackgroundControlAsset : BaseBackgroundControlAsset
    {
        /// <summary> Settings to use on the video player for the background media sphere. </summary>
        [Tooltip("Settings to use on the video player for the background media sphere.")]
        public BaseVideoAsset BackgroundVideoSettings;
    }
}