﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Scene Transition Controller")]
    public class SceneTransitionControlAsset : BaseAsset
    {
        /// <summary> How long should this scene stay up before automatically transitioning to the next? </summary>
        [Tooltip("<summary> How long should this scene stay up before automatically transitioning to the next?")]
        public float TransitionTime;

        /// <summary> Which scene does this transition to? </summary>
        [Tooltip("Which scene does this transition to?")]
        public string NextSceneName;
    }
}