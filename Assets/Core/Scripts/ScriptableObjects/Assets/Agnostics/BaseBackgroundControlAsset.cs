﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    // [CreateAssetMenu(menuName = "Kingfisher/UI/Video Background Controller")]
    public abstract class BaseBackgroundControlAsset : BaseAsset
    {
        /// <summary> Should the transition fade in, making the transition overlay opaque? </summary>
        [Tooltip("Should the transition fade in, making the transition overlay opaque?")]
        public bool FadeTransitionIn;

        /// <summary> Should the transition fade out, making the transition overlay transparent? </summary>
        [Tooltip("Should the transition fade in, making the transition overlay transparent?")]
        public bool FadeTransitionOut;

        /// <summary> If true, this control asset will change the media (implementation dependent) on scene enter. </summary>
        [Tooltip("If true, this control asset will change the media (implementation dependent) on scene enter.")]
        public bool ChangeMediaOnEnter;
    }
}