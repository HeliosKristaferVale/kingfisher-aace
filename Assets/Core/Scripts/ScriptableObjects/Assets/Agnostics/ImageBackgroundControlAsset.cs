﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Image Background Controller")]
    public class ImageBackgroundControlAsset : BaseBackgroundControlAsset
    {
        /// <summary>
        /// Texture to be placed on the media sphere when this scene is loaded. Unwrapped using
        /// the selected projection for the tour.
        /// </summary>
        [Tooltip("Texture to be placed on the media sphere.")]
        public Texture Image;
    }
}