﻿using Kingfisher.ScriptableObjects.Scenes;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Clickable Button")]
    public class ButtonAsset : BaseTransformedAsset
    {
        /// <summary>
        /// This just lets use re-use the UIButton/ButtonAsset code for more things.
        /// It is likely that you will need to add more Action types here for various
        /// requirements and then implement that action in the associated UIButton.
        /// </summary>
        public enum Action
        {
            // When pressed, this button should transition to the scene specified by the Interaction
            TransitionScene,
            // When pressed, this button should start the background video
            StartBackgroundVideo,
        }

        public Action ButtonAction;
        public string Interaction;
        public bool HideButtonAfterUse;
        public Sprite NormalState;
        public Sprite HighlightState;
        public Sprite PressedState;
        public Sprite DisabledState;
        public Sprite VisitedSprite;

        [Tooltip("If all of the scenes in the Scene List have been visited this button will automatically be disabled.")]
        public List<BaseSceneAsset> DisableIfVisitedAllScenes;

        [Tooltip("If all of the scenes in the Scene List have been visited this button will show a Visited icon on it.")]
        public List<BaseSceneAsset> MarkVisitedIfAllScenes;
    }
}