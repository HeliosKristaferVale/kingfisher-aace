﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Video (Popup)")]
    public class PopupVideoAsset : PopupImageAsset
    {
        /// <summary> Settings to use on the video player for this Popup. </summary>
        [Tooltip("Settings to use on the video player for this Popup.")]
        public BaseVideoAsset VideoSettings;

        /// <summary> Settings to use on the background audio for this Popup. </summary>
        [Tooltip("Settings to use on the background audio for this Popup.")]
        public BaseAudioDuckingAsset DuckingSettings;

        /// <summary> If true, the popup will close itself automatically when the video finishes playing. Use this if you don't want it to hold on the last frame until they open another popup or manually close it. </summary>
        [Tooltip("If true, the popup will close itself automatically when the video finishes playing. Use this if you don't want it to hold on the last frame until they open another popup or manually close it.")]
        public bool ClosePopupOnFinish = false;
    }
}