﻿using Kingfisher.ScriptableObjects.Scenes;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/Audio/Non-Looping (Start of Scene)")]
    public class SceneAudioAsset : BaseAsset
    {
        public AudioClip AudioClip;
        public float Delay;

        /// <summary> Settings to use on the background audio for this Popup. </summary>
        [Tooltip("Settings to use on the background audio for this Popup.")]
        public BaseAudioDuckingAsset DuckingSettings;

        [Tooltip("If all of the scenes in the Scene List have been visited this clip will not play.")]
        public List<BaseSceneAsset> DisableIfVisitedAllScenes;

        void OnEnable()
        {
            // Forcibly disable this (it defaults to true inside the BaseAudioDuckingAsset) 
            // because SceneAudioAssets shouldn't be cutting off other scene audio assets right now.
            DuckingSettings.MuteSceneAudio = false;
        }
    }
}