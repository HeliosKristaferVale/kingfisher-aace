﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    public abstract class BaseTransformedAsset : BaseAsset
    {
        /// <summary> [0-1] Normalized Value for X/Y Position, Relative to Top Left of Equirectangular Projection. </summary>
        [Tooltip("[0-1] Value relative to Top Left of Equirectangular Projection")]
        public Vector2 Position = new Vector2(0.5f, 0.5f);

        /// <summary> Distance in meters (to go with Position) </summary>
        [Tooltip("Distance in meters (to go with Position)")]
        public float Distance = 10f;
        
        /// <summary> Should this asset rotate to face the user horizontally? </summary>
        [Tooltip("Should this asset rotate to face the user horizontally?")]
        public bool FaceCameraHorizontally = true;

        /// <summary> Should this asset rotate to face the user vertically? Can be enabled to give the apperance of a sphere instead of cylinder. </summary>
        [Tooltip("Should this asset rotate to face the user vertically? Can be enabled to give the apperance of a sphere instead of cylinder.")]
        public bool FaceCameraVertically = false;

        /// <summary> 
        /// If supported, how long after the scene starts before showing this asset?
        /// Note: Doesn't delay creation, all assets are created at start of tour, only
        /// delays internal calls to show the asset in the scene where supported.
        /// </summary>
        [Tooltip("If supported, how long after the scene starts before showing this asset?")]
        public float ShowDelay = 0f;
    }
}