﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/Audio/Background Audio")]
    public class BackgroundAudioAsset : BaseAsset
    {
        /// <summary> What audio clip should be used for the background music in the scene? </summary>
        [Tooltip("What audio clip should be used for the background music in the scene?")]
        public AudioClip BackgroundMusic;

        /// <summary> Should the specified clip automatically fade in when entering the scene? </summary>
        [Tooltip("Should the specified clip automatically fade in when entering the scene?")]
        public bool FadeInOnEnter = true;

        /// <summary> Should the specified clip automatically fade out when exiting the scene? </summary>
        [Tooltip("Should the specified clip automatically fade out when exiting the scene?")]
        public bool FadeOutOnExit = true;

        /// <summary> Should the background music be changed to specified clip, or just left as is? </summary>
        [Tooltip("Should the background music be changed to specified clip, or just left as is?")]
        public bool ChangeTrack = true;
    }
}