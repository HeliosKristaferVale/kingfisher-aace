﻿using UnityEngine;
using UnityEngine.UI;

namespace Kingfisher.ScriptableObjects.UI
{
    public abstract class BasePopupAsset : BaseTransformedAsset
    {
        [Header("Icon")]
        public Sprite IconNormalState;
        public Sprite IconHighlightState;
        public Sprite IconPressedState;
        public Sprite IconDisabledState;

        [Header("Close Button")]
        public Sprite CloseNormalState;
        public Sprite CloseHighlightState;
        public Sprite ClosePressedState;
        public Sprite CloseDisabledState;

        public bool CloseOtherPopupsOnOpen = true;
        public bool EnableLightboxOnOpen = false;
    }
}