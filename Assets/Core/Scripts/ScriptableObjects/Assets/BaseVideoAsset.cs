﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects
{
    /// <summary>
    /// Because we have videos in several different assets (popups, static ones, background)
    /// this struct contains common functionality for *all* video playback. Specific implementations
    /// (such as Popups) may add additional functionality and thus further settings in their 
    /// own uses.
    /// </summary>
    [System.Serializable]
    public class BaseVideoAsset
    {
        /// <summary> Path of the media on the SD card, relative to Tour's folder. </summary>
        [Tooltip("Path of the media on the SD card, relative to the Tour's folder.")]
        public string MediaPath;

        /// <summary> Should the video auto-play? </summary>
        [Tooltip("Should the video auto-play?")]
        public bool AutoPlay = true;

        /// <summary> 
        /// If true, the video will automatically loop when it reaches the end. 
        /// </summary>
        [Tooltip("Should this video automatically loop?")]
        public bool Loop;
    }
}