﻿using Kingfisher.UI;
using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    public abstract class BaseAsset : ScriptableObject
    {
        /// <summary> Which prefab is automatically spawned when this asset is loaded? This asset will be passed to the prefab for further processing. </summary>
        [Tooltip("What prefab should this asset spawn when loaded?")]
        public UIBasePrefab Prefab;
    }
}