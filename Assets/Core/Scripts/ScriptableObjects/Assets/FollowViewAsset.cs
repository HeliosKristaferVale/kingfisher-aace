﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/Misc/View Follower")]
    public class FollowViewAsset : BaseAsset
    {
        /// <summary> Maximum distance in degrees the asset can get from your view. </summary>
        [Tooltip("Maximum distance in meters the asset can get from your view.")]
        public float MaximumFieldOfView = 35f;

        /// <summary> What asset should be spawned to follow the view? </summary>
        [Tooltip("What asset should be spawned to follow the view?")]
        public BaseAsset FollowingAsset;

        /// <summary> Should this asset follow the user's view vertically? </summary>
        [Tooltip("Should this asset follow the user's view vertically?")]
        public bool FollowHorizontally = true;

        /// <summary> Should this asset follow the user's view horizontally? </summary>
        [Tooltip("Should this asset follow the user's view horizontally?")]
        public bool FollowVertically = false;
    }
}