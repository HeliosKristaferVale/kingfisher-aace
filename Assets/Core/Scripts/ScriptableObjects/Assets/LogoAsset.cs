﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.UI
{
    [CreateAssetMenu(menuName = "Kingfisher/UI/Logo (Static)")]
    public class LogoAsset : BaseTransformedAsset
    {
        /// <summary> What image to show on the Logo Asset. </summary>
        [Tooltip("What image does this represent?")]
        public Sprite Logo;
    }
}