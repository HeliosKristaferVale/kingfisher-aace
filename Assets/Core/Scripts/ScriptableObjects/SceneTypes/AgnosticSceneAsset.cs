﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.Scenes
{
    /// <summary>
    /// This scene is agnostic to the control of the background. This allows you to control the
    /// background via objects in the scene, instead of via the scene itself. This gives you more
    /// control over the background and lets you do special case things like having multiple
    /// scene changes (such as nestled menus) without changing the background or fading the 
    /// transition layer in.
    /// </summary>
    [CreateAssetMenu(menuName = "Kingfisher/Scenes/Agnostic Playback Scene")]
    public class AgnosticSceneAsset : BaseSceneAsset
    {
    }
}