﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.Scenes
{
    [CreateAssetMenu(menuName = "Kingfisher/Scenes/Simple Image Scene")]
    public class ImageSceneAsset : BaseMediaPlaybackSceneAsset
    {
        /// <summary>
        /// Texture to be placed on the media sphere when this scene is loaded. Unwrapped using
        /// the selected projection for the tour.
        /// </summary>
        [Tooltip("Texture to be placed on the media sphere.")]
        public Texture Image;

        [Tooltip("If AutoTransition is true, how long to wait (in seconds) before transitioning")]
        public float AutoTransitionSceneLength;
    }
}
