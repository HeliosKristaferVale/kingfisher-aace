﻿namespace Kingfisher.ScriptableObjects.Scenes
{
    public abstract class BaseMediaPlaybackSceneAsset : BaseSceneAsset
    {
        /// <summary> If true the scene will automatically transition (implementation dependent). </summary>
        public bool AutoTransition;

        /// <summary> If <see cref="AutoTransition"/> is set to true, this is the state it will automatically transition to. </summary>
        public string NextSceneName;
    }
}