﻿using Kingfisher.ScriptableObjects.UI;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher.ScriptableObjects.Scenes
{
    public class BaseSceneAsset : ScriptableObject
    {
        public BaseStateMachineState Prefab;
        public string SceneName;
        public bool RecenterViewOnEnter;
        public List<BaseAsset> SceneAssets;
    }
}