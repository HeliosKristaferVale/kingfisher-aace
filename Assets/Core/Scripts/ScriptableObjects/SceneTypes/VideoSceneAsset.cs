﻿using UnityEngine;

namespace Kingfisher.ScriptableObjects.Scenes
{
    [CreateAssetMenu(menuName = "Kingfisher/Scenes/Simple Video Playback Scene")]
    public class VideoSceneAsset : BaseMediaPlaybackSceneAsset
    {
        /// <summary> Settings to use on the video player for the background media sphere. </summary>
        [Tooltip("Settings to use on the video player for the background media sphere.")]
        public BaseVideoAsset BackgroundVideoSettings;
    }
}