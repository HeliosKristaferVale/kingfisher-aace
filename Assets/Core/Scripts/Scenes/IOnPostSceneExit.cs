﻿namespace Kingfisher
{
    /// <summary>
    /// Components which implement this interface are called after the scene fades the transition layer in.
    /// OnPostExit is called for all components. After OnPostExit has been called ShouldBlock will be repeatedly
    /// called until all components return false. This allows a component to (optionally!) block the scene from finishing exiting.
    /// This is useful for delaying things like chapter outro icons after the scene has finished the transition color.
    /// </summary>
    public interface IOnPostSceneExit
    {
        /// <summary> 
        /// Called on all components in the scene which implement this interface.
        /// Should not do any blocking operations, if you want the intro to wait 
        /// before going further return true in <see cref="ShouldBlockPostExit"/> until done./>
        /// </summary>
        void OnPostExit();

        /// <summary>
        /// Allows the component to block the scene from finishing the scene transition after transition is faded out.
        /// Should not actually block the main thread but return true each frame until it no longer wants the background to block.
        /// </summary>
        /// <returns>True to prevent scene transition from finishing, false to allow the scene finish starting normally.</returns>
        bool ShouldBlockPostExit();
    }
}