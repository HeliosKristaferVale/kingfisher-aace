﻿namespace Kingfisher
{
    /// <summary>
    /// Components which implement this interface are called before the scene fades the transition layer out.
    /// OnPreEnter is called for all components. After OnPreEnter has been called ShouldBlock will be repeatedly
    /// called until all components return false. This allows a component to (optionally!) block the scene transition.
    /// This is useful for things like "Chapter Intros" where you want it to be visible against the transition background
    /// before actually starting the scene.
    /// </summary>
    public interface IOnPreSceneEnter
    {
        /// <summary> 
        /// Called on all components in the scene which implement this interface.
        /// Should not do any blocking operations, if you want the intro to wait 
        /// before going further return true in <see cref="ShouldBlockPreEnter"/> until done./>
        /// </summary>
        void OnPreEnter();

        /// <summary>
        /// Allows the component to block the scene from fading the transition background out.
        /// Should not actually block the main thread but return true each frame until it no longer
        /// wants the background to block.
        /// </summary>
        /// <returns>True to prevent scene transition from starting, false to allow the transition to start normally.</returns>
        bool ShouldBlockPreEnter();
    }
}