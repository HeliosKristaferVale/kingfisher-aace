﻿using System.Collections;
using Kingfisher.ScriptableObjects.Scenes;
using UnityEngine;

namespace Kingfisher.Scenes
{
    /// <summary>
    /// This handles basic image playback on the media sphere. If the sphere is not configured for image playback
    /// it will configure it upon entering the state.
    /// </summary>
    public class BaseImagePlaybackState : BaseMediaPlaybackState
    {
        protected ImageSceneAsset m_imageSceneAsset;

        public override IEnumerator OnStateEntered()
        {
            Debug.LogFormat("Entering State: \"{0}\"", SceneName);
            yield return StartCoroutine(base.OnStateEntered());

            // Ensure the Media Player is stopped so that any latent load doesn't take over the
            // media sphere again via its texture changed callback.
            m_application.MediaPlayer.Stop();
            m_application.MediaSphere.SetPlaybackTexture(m_imageSceneAsset.Image, false);

            // Show any components which implement the IPreSceneEnter interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneEnterComponents());

            m_startTime = Time.time;

            // Wait another half second before starting to fade out the background.
            // yield return new WaitForSeconds(0.25f);

            // Fade out the background and wait while we do that
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeOutCoroutine());

            // Show any components which implement the IPostSceneEnter interface and wait until they
            // are all finished. This is effectively the message sent when the scene is finally "entered",
            // though if any of the IPostSceneEnter components block then the timing may be slightly off.
            yield return StartCoroutine(ShowPostSceneEnterComponents());

            Debug.LogFormat("Finished entering State: \"{0}\"", SceneName);
        }

        public override IEnumerator OnStateExited()
        {
            Debug.LogFormat("Exiting State: \"{0}\"", SceneName);
            yield return StartCoroutine(base.OnStateExited());

            // Show any components which implement the IPreSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneExitComponents());

            // Wait for the transition layer to fade in
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeInCoroutine());

            // Show any components which implement the IPostSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPostSceneExitComponents());

            Debug.LogFormat("Finished exiting State: \"{0}\"", SceneName);
        }

        public override void LoadFromAsset(BaseSceneAsset scene)
        {
            base.LoadFromAsset(scene);

            m_imageSceneAsset = scene as ImageSceneAsset;
            if (m_imageSceneAsset == null)
                throw new System.ArgumentException("scene must be of type ImageSceneAsset!");
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            if (m_imageSceneAsset.AutoTransition)
            {
                if (Time.time - m_startTime >= m_imageSceneAsset.AutoTransitionSceneLength)
                {
                    m_application.ChangeState(m_imageSceneAsset.NextSceneName);
                }
            }
        }
    }
}