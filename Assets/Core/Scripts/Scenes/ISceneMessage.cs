﻿namespace Kingfisher
{
    public interface ISceneMessage<T>
    {
        /// <summary>
        /// This allows a somewhat flexible way of passing data from objects in the scene up to
        /// the scene that contains them. For example, you could pass a message from a button to
        /// start playing a video, or transition to the next scene. This allows you to decouple
        /// objects in the scene from knowing about specific scene types, while still sending strongly
        /// typed messages to them. This allows us a type-safe way of sending a message to our parent,
        /// without our parent knowing about the object sending the message or the object knowing the parent.
        /// 
        /// Usage:
        /// To send an event to a parent scene, an object in the scene needs to call:
        /// 
        /// var parentScene = GetComponentInParent<ISceneMessage<Foo>>
        /// if(parentScene) parentScene.HandleSceneEvent(new Foo());
        /// 
        /// Where Foo is the strongly typed event you want to send. Now this object can send Foo events to any
        /// parent scene, regardless of what the parent scene is. This allows us greater flexibility in where 
        /// objects in the scene are used. To recieve the Foo event in the parent scene, your parent scene needs to
        /// implement a strongly typed version of the generic, ie:
        /// 
        /// class BaseMediaPlaybackState : BaseStateMachineState, ISceneMessage<Foo>
        /// void HandleSceneEvent(Foo data)
        /// 
        /// This method will now get invoked by any object who looks for the ISceneMessage<Foo> implementations
        /// and invokes them.
        /// </summary>
        /// <param name="data"></param>
        void HandleSceneEvent(T data);
    }
}