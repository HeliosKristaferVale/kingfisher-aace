﻿namespace Kingfisher
{
    /// <summary>
    /// Components which implement this interface are called before the scene fades the transition layer in.
    /// OnPreExit is called for all components. After OnPreExit has been called ShouldBlock will be repeatedly
    /// called until all components return false. This allows a component to (optionally!) block the scene transition from starting.
    /// </summary>
    public interface IOnPreSceneExit
    {
        /// <summary> 
        /// Called on all components in the scene which implement this interface.
        /// Should not do any blocking operations, if you want the intro to wait 
        /// before going further return true in <see cref="ShouldBlockExit"/> until done./>
        /// </summary>
        void OnPreExit();

        /// <summary>
        /// Allows the component to block the scene from fading the transition background out.
        /// Should not actually block the main thread but return true each frame until it no longer
        /// wants the background to block.
        /// </summary>
        /// <returns>True to prevent scene transition from starting, false to allow the transition to start normally.</returns>
        bool ShouldBlockExit();
    }
}