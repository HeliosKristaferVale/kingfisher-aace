﻿using System.Collections;
using Kingfisher.Scenes.Events;
using Kingfisher.ScriptableObjects.Scenes;
using UnityEngine;

namespace Kingfisher.Scenes
{
    /// <summary>
    /// This handles basic functionality for media playback on the media sphere. It can be overwritten to
    /// provdies specific functionality such as Images or Videos.
    /// </summary>
    public abstract class BaseMediaPlaybackState : BaseStateMachineState, ISceneMessage<SceneTransitionEvent>
    {
        protected float m_startTime;

        protected virtual IEnumerator ShowPreSceneEnterComponents()
        {
            var preSceneEnterComponents = GetComponentsInChildren<IOnPreSceneEnter>(true);

            // Notify all components at once that we are entering
            foreach (var component in preSceneEnterComponents)
                component.OnPreEnter();

            // Now yield every frame until no component reports that it wants to continue blocking.
            bool bIsBlocked = true;
            while(bIsBlocked)
            {
                bIsBlocked = false;

                foreach (var component in preSceneEnterComponents)
                {
                    if (component.ShouldBlockPreEnter())
                    {
                        bIsBlocked = true;
                        break;
                    }
                }

                if (bIsBlocked)
                    yield return null;
            }
        }

        protected virtual IEnumerator ShowPostSceneEnterComponents()
        {
            var postSceneEnterComponents = GetComponentsInChildren<IOnPostSceneEnter>(true);

            // Notify all components at once that we are entering
            foreach (var component in postSceneEnterComponents)
                component.OnPostEnter();

            // Now yield every frame until no component reports that it wants to continue blocking.
            bool bIsBlocked = true;
            while (bIsBlocked)
            {
                bIsBlocked = false;

                foreach (var component in postSceneEnterComponents)
                {
                    if (component.ShouldBlockEnter())
                    {
                        bIsBlocked = true;
                        break;
                    }
                }

                if (bIsBlocked)
                    yield return null;
            }
        }

        protected virtual IEnumerator ShowPreSceneExitComponents()
        {
            var preSceneExitComponents = GetComponentsInChildren<IOnPreSceneExit>(true);

            // Notify all components at once that we are entering
            foreach (var component in preSceneExitComponents)
                component.OnPreExit();

            // Now yield every frame until no component reports that it wants to continue blocking.
            bool bIsBlocked = true;
            while (bIsBlocked)
            {
                bIsBlocked = false;

                foreach (var component in preSceneExitComponents)
                {
                    if (component.ShouldBlockExit())
                    {
                        bIsBlocked = true;
                        break;
                    }
                }

                if (bIsBlocked)
                    yield return null;
            }
        }

        protected virtual IEnumerator ShowPostSceneExitComponents()
        {
            var postSceneExitComponents = GetComponentsInChildren<IOnPostSceneExit>(true);

            // Notify all components at once that we are entering
            foreach (var component in postSceneExitComponents)
                component.OnPostExit();

            // Now yield every frame until no component reports that it wants to continue blocking.
            bool bIsBlocked = true;
            while (bIsBlocked)
            {
                bIsBlocked = false;

                foreach (var component in postSceneExitComponents)
                {
                    if (component.ShouldBlockPostExit())
                    {
                        bIsBlocked = true;
                        break;
                    }
                }

                if (bIsBlocked)
                    yield return null;
            }
        }

        /// <summary>
        /// Called when a object in the scene wants to invoke a transition to a specific scene.
        /// </summary>
        /// <param name="data"></param>
        public void HandleSceneEvent(SceneTransitionEvent data)
        {
            string sceneName = data.DestinationSceneOverride;

            // If the event doesn't explicitly specify a scene, they might be trying to use the scene's
            // default next scene. However, the concept of a 'NextScene' is not supported on all scene
            // types - so we'll cast and check if we can get that data.
            if (string.IsNullOrEmpty(sceneName))
            {
                var mediaPlaybackSceneAsset = m_baseSceneAsset as BaseMediaPlaybackSceneAsset;
                if (mediaPlaybackSceneAsset != null)
                {
                    sceneName = mediaPlaybackSceneAsset.NextSceneName;
                }
            }

            if (!string.IsNullOrEmpty(sceneName))
            {
                m_application.ChangeState(sceneName);
            }
            else
            {
                // This means someone sent a SceneTransitionEvent with no scene specified to a 
                // scene that doesn't derive from BaseMediaPlaybackSceneAsset, so we have no idea
                // where to go.
                string warnMsg =
                    "Recieved HandleSceneEvent but the asset wasn't a BaseMediaPlaybackSceneAsset. " +
                    "This means that this scene is skipping the event. Most likely it is because you are " +
                    "using a Agnostic Scene (which doesn't support auto-transitions). So we're just warning you " +
                    "that this isn't going to work as expected. Why? Mistakes were made.";
                Debug.LogWarning(warnMsg);
            }
        }
    }
}