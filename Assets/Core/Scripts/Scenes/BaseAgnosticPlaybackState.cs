﻿using System.Collections;
using Kingfisher.ScriptableObjects.Scenes;
using UnityEngine;

namespace Kingfisher.Scenes
{
    /// <summary>
    /// This is intended for 'complex' scene usages. This allows you to use certain in-scene <see cref="BaseAsset"/>
    /// to control the background media and transitions. This decouples the need to fade between background images/videos
    /// each time we wish to remove all content in a scene.
    /// 
    /// In practice this (when combined with specific in-scene objects) will allow you to create 'complex' menus
    /// that have multiple pages or states without changing the background which allows it to appear like one scene.
    /// The only caveat is because we are still entering and exiting scenes, the contents of the scene must fully disappear
    /// as the GameObject containing the content gets turned off when the scene has exited.
    /// </summary>
    public class BaseAgnosticPlaybackState : BaseMediaPlaybackState
    {
        public override IEnumerator OnStateEntered()
        {
            Debug.LogFormat("Entering State: \"{0}\"", SceneName);
            yield return StartCoroutine(base.OnStateEntered());

            // Show any components which implement the IPreSceneEnter interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneEnterComponents());

            m_startTime = Time.time;

            // Show any components which implement the IPostSceneEnter interface and wait until they
            // are all finished. This is effectively the message sent when the scene is finally "entered",
            // though if any of the IPostSceneEnter components block then the timing may be slightly off.
            yield return StartCoroutine(ShowPostSceneEnterComponents());

            Debug.LogFormat("Finished entering State: \"{0}\"", SceneName);
        }

        public override IEnumerator OnStateExited()
        {
            Debug.LogFormat("Exiting State: \"{0}\"", SceneName);
            yield return StartCoroutine(base.OnStateExited());

            // Show any components which implement the IPreSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneExitComponents());

            // Show any components which implement the IPostSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPostSceneExitComponents());

            Debug.LogFormat("Finished exiting State: \"{0}\"", SceneName);
        }
    }
}