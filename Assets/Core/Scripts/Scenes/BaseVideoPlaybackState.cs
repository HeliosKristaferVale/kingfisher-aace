﻿using Kingfisher.ScriptableObjects.Scenes;
using Kingfisher.UI;
using System.Collections;
using UnityEngine;
using System;
using Kingfisher.Scenes.Events;

namespace Kingfisher.Scenes
{
    /// <summary>
    /// This handles basic 360 video playback on the media sphere. If the media sphere is currently configured
    /// for images, it will configure it for videos when the state is entered. It does not restore the state
    /// upon exit.
    /// </summary>
    public class BaseVideoPlaybackState : BaseMediaPlaybackState, ISceneMessage<BackgroundVideoEvent>
    {
        protected VideoSceneAsset m_videoSceneAsset;

        public override IEnumerator OnStateEntered()
        {
            Debug.LogFormat("Entering State: \"{0}\"", SceneName);
            var videoSettings = m_videoSceneAsset.BackgroundVideoSettings;
            if (string.IsNullOrEmpty(videoSettings.MediaPath))
                throw new System.ArgumentException("VideoPlaybackScene must specify a MediaPath.");

            yield return StartCoroutine(base.OnStateEntered());

            // Immediately trigger our video to load, which can take several frames.
            m_application.MediaPlayer.SetLoops(videoSettings.Loop);
            m_application.MediaPlayer.LoadMediaAsync(videoSettings.MediaPath, OnMediaFinishedLoading);

            if(videoSettings.Loop && m_videoSceneAsset.AutoTransition)
                Debug.LogWarningFormat("State: \"{0}\" has both Loops and AutoTransition set, AutoTransition will be ignored!", SceneName);

            Debug.LogFormat("Finished entering State: \"{0}\"", SceneName);
        }

        /// <summary>
        /// This is called automatically when the native plugin reports that the video you requested to load has finished loading.
        /// </summary>
        protected virtual void OnMediaFinishedLoading()
        {
            StartCoroutine(OnMediaLoadedCoroutine());
        }

        protected virtual IEnumerator OnMediaLoadedCoroutine()
        {
            Debug.LogFormat("Media finished loading for State: \"{0}\", doing intro...", SceneName);

            // Show any components which implement the IPreSceneEnter interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneEnterComponents());

            m_startTime = Time.time;

            if (m_videoSceneAsset.BackgroundVideoSettings.AutoPlay)
            {
                SetVideoIsPlaying(true);
            }

            // Fade out the background and wait while we do that
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeOutCoroutine());

            // Show any components which implement the IPostSceneEnter interface and wait until they
            // are all finished. This is effectively the message sent when the scene is finally "entered",
            // though if any of the IPostSceneEnter components block then the timing may be slightly off.
            yield return StartCoroutine(ShowPostSceneEnterComponents());

            Debug.LogFormat("Finished intro for State: \"{0}\"", SceneName);
        }

        /// <summary>
        /// This assumes you have already had the OnMediaFinishedLoading callback fired.
        /// </summary>
        private void SetVideoIsPlaying(bool isPlaying)
        {
            // Start the video in the background
            Debug.LogFormat("State: \"{0}\" asking media player to change state. Playing: {1}", SceneName, isPlaying);
            if (isPlaying)
                m_application.MediaPlayer.Play();
            else
                m_application.MediaPlayer.Pause();
            Debug.LogFormat("State: \"{0}\" Media Player should have changed state by now.", SceneName);
        }

        public override IEnumerator OnStateExited()
        {
            Debug.LogFormat("Exiting State: \"{0}\"", SceneName);
            yield return StartCoroutine(base.OnStateExited());

            // Show any components which implement the IPreSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPreSceneExitComponents());

            // Wait for the transition layer to fade in
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeInCoroutine());
            m_application.MediaPlayer.Stop();

            // Show any components which implement the IPostSceneExit interface and wait until they all finished.
            yield return StartCoroutine(ShowPostSceneExitComponents());

            Debug.LogFormat("Finished exiting State: \"{0}\"", SceneName);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            // The scene is technically live (and thus ticking) while the media is loading, 
            // so we're going to wait until the media player is playing + we have the metadata 
            // about duration. Some platforms don't return duration immediately when we start playing!
            if (!m_application.MediaPlayer.IsPlaying || m_application.MediaPlayer.DurationInSeconds == 0f)
                return;

            // On GearVR they can hold down the back button on the device to skip to the next scene.
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape))
            {
                Debug.LogFormat("Exiting Scene \"{0}\" because Space or Escape pressed.", SceneName);
                m_application.ChangeState(m_videoSceneAsset.NextSceneName);
                return;
            }

            // If we're set to auto-loop we ignore auto-transition.
            if(!m_videoSceneAsset.BackgroundVideoSettings.Loop && m_videoSceneAsset.AutoTransition)
            {
                if (m_application.MediaPlayer.DurationInSeconds != 0f)
                {
                    float timeRemaining = m_application.MediaPlayer.DurationInSeconds - m_application.MediaPlayer.CurrentTimeInSeconds;

                    // Automatically trigger our state exit when our video has almost run out of time. We need to do this
                    // so that we don't run out of footage to show as the transition fades in.
                    if (timeRemaining <= m_application.Transition.TransitionDurationSeconds)
                    {
                        Debug.LogFormat("Exiting Scene \"{0}\" because autoTransition enabled and the transition should start now.", SceneName);
                        m_application.ChangeState(m_videoSceneAsset.NextSceneName);
                    }
                }
            }
        }

        public override void LoadFromAsset(BaseSceneAsset scene)
        {
            base.LoadFromAsset(scene);

            m_videoSceneAsset = scene as VideoSceneAsset;
            if (m_videoSceneAsset == null)
                throw new System.ArgumentException("scene must be of type SimpleVideoPlaybackSceneScriptableObject!", "scene");
        }

        void ISceneMessage<BackgroundVideoEvent>.HandleSceneEvent(BackgroundVideoEvent data)
        {
            SetVideoIsPlaying(data.VideoShouldPlay);
        }
    }
}