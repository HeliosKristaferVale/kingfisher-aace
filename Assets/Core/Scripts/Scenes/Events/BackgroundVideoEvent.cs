﻿namespace Kingfisher.Scenes.Events
{
    public struct BackgroundVideoEvent
    {
        /// <summary> Start or stop the background video. </summary>
        public bool VideoShouldPlay;

        public BackgroundVideoEvent(bool videoShouldPlay)
        {
            VideoShouldPlay = videoShouldPlay;
        }
    }
}