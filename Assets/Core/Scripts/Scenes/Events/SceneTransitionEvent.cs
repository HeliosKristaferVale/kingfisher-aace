﻿namespace Kingfisher.Scenes.Events
{
    public struct SceneTransitionEvent
    {
        /// <summary> If this is not empty, the scene we will transition to. Otherwise we go to the scene's default next scene (if supported) </summary>
        public string DestinationSceneOverride;

        public SceneTransitionEvent(string sceneNameOverride = "")
        {
            DestinationSceneOverride = sceneNameOverride;
        }
    }
}