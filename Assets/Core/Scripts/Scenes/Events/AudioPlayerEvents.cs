﻿using UnityEngine;

namespace Kingfisher.Scenes.Events
{
    public struct AudioPlayerVolumeEvent
    {
        /// <summary> What is the new volume the audio player should adjust to? [0-1] range. </summary>
        public float NewVolume;

        /// <summary> How fast should the audio tween in units per second? (ie: 2 means it will take a half second to change) </summary>
        public float TweenRate;

        public AudioPlayerVolumeEvent(float newVolume, float tweenRate = 1f)
        {
            NewVolume = newVolume;
            TweenRate = tweenRate;
        }
    }

    public struct AudioPlayerTrackChangeEvent
    {
        /// <summary> What audio clip should the audio player change to? Will try to smoothly fade between them. </summary>
        public AudioClip NewTrack;

        public AudioPlayerTrackChangeEvent(AudioClip newTrack)
        {
            NewTrack = newTrack;
        }
    }

    public struct AudioPlayerDuckEvent
    {
        /// <summary> If true, the background music will get ducked. </summary>
        public bool IsDucked;

        /// <summary> How much to duck the audio in db </summary>
        public float DuckAmount;

        public AudioPlayerDuckEvent(bool isDucked, float duckAmount = -12f)
        {
            IsDucked = isDucked;
            DuckAmount = duckAmount;
        }
    }
}