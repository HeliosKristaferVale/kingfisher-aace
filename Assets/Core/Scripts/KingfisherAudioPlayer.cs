﻿using Kingfisher.Scenes.Events;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Audio;

namespace Kingfisher
{
    [RequireComponent(typeof(AudioSource))]
    public class KingfisherAudioPlayer : MonoBehaviour, ISceneMessage<AudioPlayerVolumeEvent>, ISceneMessage<AudioPlayerTrackChangeEvent>, ISceneMessage<AudioPlayerDuckEvent>
    {
        [SerializeField] private AudioMixer m_mixer;

        private AudioSource m_audioSource;

        protected virtual void Awake()
        {
            m_audioSource = GetComponent<AudioSource>();
            m_audioSource.loop = true;
        }

        void ISceneMessage<AudioPlayerTrackChangeEvent>.HandleSceneEvent(AudioPlayerTrackChangeEvent data)
        {
            m_audioSource.clip = data.NewTrack;
            m_audioSource.Play();
        }

        void ISceneMessage<AudioPlayerVolumeEvent>.HandleSceneEvent(AudioPlayerVolumeEvent data)
        {
            m_audioSource.DOKill();
            m_audioSource.DOFade(data.NewVolume, data.TweenRate).SetSpeedBased();
        }

        void ISceneMessage<AudioPlayerDuckEvent>.HandleSceneEvent(AudioPlayerDuckEvent data)
        {
            float duckedVolume = data.IsDucked ? data.DuckAmount : 0f;
            Debug.LogFormat("AudioDucking to Volume: {0}", duckedVolume);

            m_mixer.DOKill();
            m_mixer.DOSetFloat("Background.Volume", duckedVolume, 1f).SetSpeedBased();
        }
    }
}