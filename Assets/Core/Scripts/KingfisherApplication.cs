﻿using Kingfisher.Permissions;
using Kingfisher.ScriptableObjects;
using Kingfisher.ScriptableObjects.Scenes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Kingfisher.UI;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

namespace Kingfisher
{
    public class KingfisherApplication : FiniteStateMachine
    {
        public KingfisherMediaPlayer MediaPlayer { get { return m_mediaPlayer; } }
        public VideoTransitionMesh Transition { get { return m_transitionSphere; } }
        public VideoPlaybackMesh MediaSphere { get { return m_videoSphere; } }
        public Camera HMD { get { return m_vrCameraRig.UserCamera; } }

        [Header("SET THIS WHEN MAKING A NEW TOUR")]
        [SerializeField] private VRTourAsset m_vrTour;

        [Header("References")]
        [SerializeField] private VideoPlaybackMesh m_videoSphere;
        [SerializeField] private VideoTransitionMesh m_transitionSphere;
        [SerializeField] private VRCameraRigSpawner m_vrCameraRig;
        [SerializeField] private Text m_globalErrorText;

        [SerializeField] private GameObject m_mediaPlayerGameObject;
        private KingfisherMediaPlayer m_mediaPlayer;
        [SerializeField] private AudioClip[] m_audioPreload;

        [Header("GearVR Settings")]
#pragma warning disable 0414 // We use this on GEAR_VR combinations and want them always serialized.
        [SerializeField] private float m_vrRenderScale = 1.5f;
        [SerializeField] private int m_defaultCPULevel = 1;
        [SerializeField] private int m_defaultGPULevel = 2;
#pragma warning restore 0414


        protected Transform m_sceneHolder;
        protected List<string> m_visitedScenes;

#pragma warning disable 0414 // Used on GOOGLE_DAYDREAM combinations and want them always serialized.
        // Check for triple-click on controller for experience soft-reset.
        float m_timeSinceLastClick;
        float m_tripleClickThreshold = 0.25f;
        int m_clicksUntilReset;
#pragma warning restore 0414

        protected override void Awake()
        {
#if GEAR_VR
        // Set up some default VR settings which are tweaked for Quality/Performance/Batterylife improvements.
        UnityEngine.XR.XRSettings.eyeTextureResolutionScale = m_vrRenderScale;
        //OVRManager.cpuLevel = m_defaultCPULevel; // This has been deprecated.
        //OVRManager.gpuLevel = m_defaultGPULevel; // This has been deprecated.
#endif
            m_visitedScenes = new List<string>();
            m_mediaPlayer = UIPopupFactory.CreateVideoPopup(m_mediaPlayerGameObject);
            m_mediaPlayer.OnPlaybackTextureChanged += m_videoSphere.SetPlaybackTexture;
            m_transitionSphere.FadeIn(true);
            m_sceneHolder = new GameObject("Scenes").transform;
            m_sceneHolder.SetParent(transform, false);

            // Hide our error text until we know if there is an error or not.
            m_globalErrorText.gameObject.SetActive(false);

            InitializeExperienceFromAsset(m_vrTour);

            // Kick off a permissions request check. This will request the permissions if not already granted
            StartCoroutine(DoPermissionsRequestAfterDelay());

            // Now that we've spawned all items 
            foreach (var comp in GetComponentsInChildren<IStateMachineReciever>(true))
                comp.SetStateMachine(this);
            
            // We run the base class's Awake function last so that it can pick up all of the components
            // we spawned above.
            base.Awake();
        }

        protected virtual void OnApplicationFocus(bool bFocused)
        {
            // Hide our cursor for recording preview videos.
            Cursor.visible = !bFocused;
        }

        protected virtual void Start()
        {
            if (m_vrTour.DefaultState == null)
                throw new System.ArgumentNullException("DefaultState", "DefaultState cannot be null. Please assign via the inspector in this tour!");

            ChangeState(m_vrTour.DefaultState.SceneName);
        }

        protected override void Update()
        {
            base.Update();

            // Detect triple-clicks for application reset.
#if GOOGLE_DAYDREAM
            if(GvrControllerInput.AppButtonDown)
            {
                float clickDelta = Time.time - m_timeSinceLastClick;
                m_timeSinceLastClick = Time.time;

                if(clickDelta < m_tripleClickThreshold)
                {
                    m_clicksUntilReset--;
                    if(m_clicksUntilReset == 0)
                    {
                        Debug.Log("Application detected triple click, soft-resetting experience to \"Intro\" state.");
                        SoftResetExperience();
                    }
                }
                else
                {
                    m_clicksUntilReset = 2;
                }
            }
#endif
        }

        public void SoftResetExperience()
        {
            // Soft reset the experience by transitioning them to the default state again.
            m_visitedScenes.Clear();
            ChangeState(m_vrTour.DefaultState.SceneName);
        }

        public void RecenterView()
        {
            m_vrCameraRig.RecenterView();
        }

        public void ResetVisitedScenes()
        {
            m_visitedScenes.Clear();
        }

        public void MarkSceneAsVisited(string scene)
        {
            m_visitedScenes.Add(scene);
        }

        public bool HasSceneBeenVisited(string sceneName)
        {
            for(int i = 0; i < m_visitedScenes.Count; i++)
            {
                if (m_visitedScenes[i] == sceneName)
                    return true;
            }

            return false;
        }

        protected virtual void InitializeExperienceFromAsset(VRTourAsset tour)
        {
            // Pre-allocate and spawn all of the required game objects and assets as required.
            if (tour.Reticle != null)
            {
                Reticle prefab = tour.Reticle.Prefab;
                if (prefab == null)
                    throw new System.ArgumentException("Reticle must have a valid prefab!", "prefab");

                Reticle reticle = Instantiate(prefab);
                reticle.transform.SetParent(HMD.transform, false);
                reticle.LoadFromAsset(tour.Reticle);
            }

            m_transitionSphere.SetTransitionColor(tour.TransitionColor);
            m_videoSphere.SetProjectionFormat(tour.VideoProjectionFormat);

            bool bContainsDefaultState = false;
            foreach (var scene in m_vrTour.VRTourSceneList)
            {
                if (scene == m_vrTour.DefaultState)
                {
                    bContainsDefaultState = true;
                    break;
                }
            }

            if (!bContainsDefaultState)
                throw new System.ArgumentException("Tour's DefaultState must also be included in the VRTourSceneList so that it gets correctly set up in-game! This might also mean that you forgot to change which VR Tour is assigned to the Kingfisher GameObject in the scene.");

            // Spawn Scenes
            for (int i = 0; i < tour.VRTourSceneList.Length; i++)
            {
                BaseSceneAsset tourScene = tour.VRTourSceneList[i];
                if(tourScene == null)
                {
                    Debug.LogWarning("null scene in tour Scene List, skipping...");
                    continue;
                }
                BaseStateMachineState prefab = tourScene.Prefab;
                if (prefab == null)
                    throw new System.ArgumentException("Scene must have a valid prefab!", "prefab");

                BaseStateMachineState scene = Instantiate(prefab);
                scene.transform.SetParent(m_sceneHolder, false);
                scene.LoadFromAsset(tourScene);
            }
        }

        #region Preload Assets
        public void PreloadAssets(Action<int, int> progressUpdate, Action onComplete)
        {
            StartCoroutine(PreloadAssets_Internal(progressUpdate, onComplete));
        }

        private IEnumerator PreloadAssets_Internal(Action<int, int> progressUpdate, Action onComplete)
        {
            Debug.LogFormat("Beginning to Preload Assets...");
            // Yield immediately so that we return execution to the main logic immediately.
            yield return null;

            Debug.LogFormat("Preloading {0} AudioClips...", m_audioPreload.Length);

            for (int i = 0; i < m_audioPreload.Length; i++)
            {
                AudioClip clip = m_audioPreload[i];
                if (clip == null)
                    continue;

                clip.LoadAudioData();

#pragma warning disable 0618
                // isReadyToPlay has been marked as obsolete... It's also the only version of the functionality that works on Android.
                while (!clip.isReadyToPlay)
                {
                    yield return null;
                }
#pragma warning restore 0618

                if (progressUpdate != null)
                    progressUpdate.Invoke(i, m_audioPreload.Length);

                // Wait a beat between each load.
                yield return null;
            }

            Debug.LogFormat("Finished Preloading {0} AudioClips!", m_audioPreload.Length);
            Debug.LogFormat("Finished Preloading Assets.");

            // Notify whoever invoked us that we're done.
            if (onComplete != null)
                onComplete.Invoke();
        }
        #endregion

        #region Permissions Flow
        private IEnumerator DoPermissionsRequestAfterDelay()
        {
            // We wait a few moments before showing the request as doing it immediately after
            // Awake or Start seems to cause a NullReferenceException in native Android code. Oops!
            yield return new WaitForSeconds(2.5f);
            CheckAndRequestPermissionsIfNeeded();
        }

        protected virtual void CheckAndRequestPermissionsIfNeeded()
        {
#if UNITY_HAS_GOOGLEVR && UNITY_ANDROID && !UNITY_EDITOR
            IVRPermissionRequest request = new DaydreamPermissionRequester();
#else
            IVRPermissionRequest request = new DummyPermissionRequester();
#endif
            // See if they have the requested permissions for that platform, and only trigger the OS request if they don't.
            if (!request.HasPermissions())
            {
                request.RequestPermissions(OnPermissionRequestCompleted);
            }
            else
            {
                // Permissions have all been granted, just call the same function.
                OnPermissionRequestCompleted(true);
            }
        }

        /// <summary>
        /// This is called when the native OS permission request completes which may be the same frame,
        /// or several frames later.
        /// </summary>
        protected virtual void OnPermissionRequestCompleted(bool hasPermission)
        {
            // We should have permissions now, attempt to check the content directory for existance to ensure all required data has been installed correctly.
            bool dirExists = true;
            try
            {
                string directoryName = DoPathFixup("");
                dirExists = System.IO.Directory.Exists(directoryName);
                Debug.LogFormat("Directory Check checking to see if SDCard content has been installed. Checking {0} Result: {1}", directoryName, dirExists);
            }
            catch(Exception ex)
            {
                Debug.LogErrorFormat("OnPermissionRequestCompleted caught exception while checking SD card. Exception:\n{0}", ex);
            }

            dirExists = dirExists && hasPermission;
            string errorText = dirExists ? "" : string.Format("Failed to find directory \"{0}\" on SD card.\nEither missing or no SD Card permissions!", m_vrTour.SDCardRootFolder);
            m_globalErrorText.text = errorText;
            m_globalErrorText.gameObject.SetActive(!dirExists);
        }
        #endregion

        public virtual string DoPathFixup(string path)
        {
#if UNITY_EDITOR
            string fixedPath = Application.dataPath + "../../sdcard/" + m_vrTour.SDCardRootFolder + "/" + path;
#else
           string fixedPath = "/sdcard/" + m_vrTour.SDCardRootFolder + "/" + path;
#endif
            // Resolve the ".." into actual folder-ups to simplify the path.
            string resolvedPath = System.IO.Path.GetFullPath(fixedPath);
            Debug.LogFormat("DoPathFixup turning path \"{0}\" into path \"{1}\"", path, resolvedPath);
            return resolvedPath;
        }
    }
}
