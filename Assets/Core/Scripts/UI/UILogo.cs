﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kingfisher.UI
{
    public class UILogo : UIBaseScenePrefab
    {
        [SerializeField] private Image m_displayImage;
        private bool m_faceCameraHorizontally;
        private bool m_faceCameraVertically;

        public override void LoadFromAsset(BaseAsset asset)
        {
            var logoAsset = asset as LogoAsset;
            if (logoAsset == null)
                throw new ArgumentException("asset is expected to be of type LogoAsset");

            m_displayImage.sprite = logoAsset.Logo;
            m_displayImage.SetNativeSize();

            Vector3 spawnPos = ConvertUVCoordToDirection(logoAsset.Position) * logoAsset.Distance;
            transform.position = spawnPos;

            m_faceCameraHorizontally = logoAsset.FaceCameraHorizontally;
            m_faceCameraVertically = logoAsset.FaceCameraVertically;

            UpdateSceneTransform();
        }

        public override void UpdateSceneTransform()
        {
            base.UpdateSceneTransform();

            RotateTransformToFaceCamera(transform, m_faceCameraHorizontally, m_faceCameraVertically);
        }
    }
}