﻿using Kingfisher.Scenes.Events;
using Kingfisher.ScriptableObjects.UI;
using System;

namespace Kingfisher.UI.Scenes
{
    public class UIBackgroundAudio : UIBasePrefab, IOnPostSceneEnter, IOnPreSceneExit
    {
        void IOnPostSceneEnter.OnPostEnter()
        {
            // Swap to our new AudioClip immediately upon entering.
            BackgroundAudioAsset baa = m_asset as BackgroundAudioAsset;
            if (baa.ChangeTrack)
            {
                var trackChangeEvent = GetComponentInParent<ISceneMessage<AudioPlayerTrackChangeEvent>>();
                if (trackChangeEvent != null)
                {
                    trackChangeEvent.HandleSceneEvent(new AudioPlayerTrackChangeEvent(baa.BackgroundMusic));
                }
            }

            if (baa.FadeInOnEnter)
            {
                // Fade in audio
                var volumeEvent = GetComponentInParent<ISceneMessage<AudioPlayerVolumeEvent>>();
                if (volumeEvent != null)
                {
                    volumeEvent.HandleSceneEvent(new AudioPlayerVolumeEvent(1f));
                }
            }
        }

        bool IOnPostSceneEnter.ShouldBlockEnter()
        {
            return false;
        }

        void IOnPreSceneExit.OnPreExit()
        {
            BackgroundAudioAsset baa = m_asset as BackgroundAudioAsset;
            if(baa.FadeOutOnExit)
            {
                // Fade out the audio
                var startVideoEvent = GetComponentInParent<ISceneMessage<AudioPlayerVolumeEvent>>();
                if (startVideoEvent != null)
                {
                    startVideoEvent.HandleSceneEvent(new AudioPlayerVolumeEvent(0f));
                }
            }
        }

        bool IOnPreSceneExit.ShouldBlockExit()
        {
            return false;
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            if((m_asset as BackgroundAudioAsset) == null)
            { 
                throw new ArgumentException("asset is expected to be of type BackgroundAudioAsset");
            }
        }
    }
}
