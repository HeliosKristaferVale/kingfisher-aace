﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher.UI.Scenes
{
    public class UITransitionFadeOut : UIBasePrefab//, IOnPostSceneEnter, IOnPostSceneExit
    {
        // PsuedoCode;
        // OnSceneEnter, do the stuff the scene normally does.
        // OnSceneExit, do the stuff the scene normally does.
        // Need a way to prevent the scene from changing the background
        // Can we merge Image + Video into a subclass
        // And have the base class 'enter' the scene, which triggers these
        // objects to play the real enter?
        // Though if they use UI objects instead, they should probably duplicate
        // the functionality of image/video scenes, and then have a dummy or base class
        // that just triggers these guys (if desired). 
        //
        // Parent state needs to call the OnSceneEnter/OnSceneExit events, which this object,
        // if spawned, needs to capture. So they need a base scene state that doesn't do anything with the background
        // by default. The Image and Video states are a concrete implementation that should be totally *separate* from
        // these guys - they should probably warn if they find them used in conjunction.
        // Can we just have the concrete implementations hook these objects?
    }
}
