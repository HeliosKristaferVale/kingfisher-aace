﻿using System;
using Kingfisher.UI;
using UnityEngine;
using System.Collections;

namespace Kingfisher.ScriptableObjects.UI.Agnostic
{
    /// <summary>
    /// This handles basic image playback on the media sphere. If the sphere is not configured for image playback
    /// it will configure it upon entering the state.
    /// </summary>
    public class UIBackgroundController : UIBasePrefab, IOnPreSceneEnter, IOnPostSceneExit
    {
        private bool m_blockPostExit = false;

        // We can block on both transition or video, in case one of them takes longer.
        private bool m_blockOnTransitionIn;
        private bool m_blockOnVideoLoad;

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            if((m_asset as BaseBackgroundControlAsset) == null)
                Debug.LogWarningFormat("Asset for UIBackgroundController needs to be of type BaseBackgroundControlAsset!");
        }

        void IOnPreSceneEnter.OnPreEnter()
        {
            // Reset our blocking state
            m_blockOnTransitionIn = false;
            m_blockOnVideoLoad = false;

            var controlAsset = m_asset as BaseBackgroundControlAsset;

            // Change the media for the background (if desired)
            if (controlAsset.ChangeMediaOnEnter)
            {
                // I was lazy and made both control methods use one UI component so there's less prefabs...
                // Also, these are both duplicates of what the BaseVideoPlaybackState / BaseImagePlaybackState
                // classes do, but because of how they're arranged and time crunch, I'm just duplicating the code.
                // Sorry :(
                var imageControl = m_asset as ImageBackgroundControlAsset;
                var videoControl = m_asset as VideoBackgroundControlAsset;

                if (imageControl != null)
                {
                    // Ensure the Media Player is stopped so that any latent load doesn't take over the
                    // media sphere again via its texture changed callback.
                    m_application.MediaPlayer.Stop();
                    m_application.MediaSphere.SetPlaybackTexture(imageControl.Image, false);
                }
                else if (videoControl != null)
                {
                    // We need to block until we finish the async video load so that it doesn't start fading out
                    // while still showing the old texture. This won't get set to false until video is loaded.
                    m_blockOnVideoLoad = true;

                    var videoSettings = videoControl.BackgroundVideoSettings;
                    if (string.IsNullOrEmpty(videoSettings.MediaPath))
                        throw new System.ArgumentException("VideoBackgroundControlAsset must specify a MediaPath.");

                    m_application.MediaPlayer.SetLoops(videoSettings.Loop);

                    // Trigger our video to load, which can take several frames.
                    m_application.MediaPlayer.LoadMediaAsync(videoSettings.MediaPath, OnMediaFinishedLoading);
                }
            }
            
            // Change the transition sphere (if desired)
            if(controlAsset.FadeTransitionOut)
            {
                // Fade out the background - when complete the coroutine will mark us as no longer blocking.
                m_blockOnTransitionIn = true;
                StartCoroutine(DoTransitionSphereOut());
            }
        }

        void IOnPostSceneExit.OnPostExit()
        {
            m_blockPostExit = false;
            var controlAsset = m_asset as BaseBackgroundControlAsset;
            
            // Change the transition sphere (if desired)
            if (controlAsset.FadeTransitionIn)
            {
                m_blockPostExit = true;
                // Fade in the background - when complete the coroutine will mark us as no longer blocking.
                StartCoroutine(DoTransitionSphereIn());
            }
        }

        bool IOnPreSceneEnter.ShouldBlockPreEnter()
        {
            return m_blockOnVideoLoad || m_blockOnTransitionIn;
        }

        bool IOnPostSceneExit.ShouldBlockPostExit()
        {
            return m_blockPostExit;
        }

        /// <summary>
        /// This assumes you have already had the OnMediaFinishedLoading callback fired.
        /// </summary>
        protected void SetVideoIsPlaying(bool isPlaying)
        {
            // Start the video in the background
            Debug.LogFormat("UIBackgroundController: \"{0}\" asking media player to change state. Playing: {0}", gameObject.name, isPlaying);
            if (isPlaying)
                m_application.MediaPlayer.Play();
            else
                m_application.MediaPlayer.Pause();
        }

        private void OnMediaFinishedLoading()
        {
            // Once the video has finished loading we no longer need to block us from entering the
            // parent scene.
            m_blockOnVideoLoad = false;
            var videoControl = m_asset as VideoBackgroundControlAsset;
            if (videoControl)
            {
                if (videoControl.BackgroundVideoSettings.AutoPlay)
                {
                    SetVideoIsPlaying(true);
                }
            }
        }

        protected IEnumerator DoTransitionSphereOut()
        {
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeOutCoroutine());
            m_blockOnTransitionIn = false;
        }

        protected IEnumerator DoTransitionSphereIn()
        {
            yield return StartCoroutine(m_application.Transition.DoTransitionFadeInCoroutine());
            m_blockPostExit = false;
        }
    }
}