﻿using System;
using Kingfisher.UI;
using UnityEngine;
using System.Collections;

namespace Kingfisher.ScriptableObjects.UI.Agnostic
{
    /// <summary>
    /// This handles basic image playback on the media sphere. If the sphere is not configured for image playback
    /// it will configure it upon entering the state.
    /// </summary>
    public class UITransitionController : UIBasePrefab, IOnPostSceneEnter
    {
        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            if((m_asset as SceneTransitionControlAsset) == null)
                Debug.LogWarningFormat("Asset for UIBackgroundController needs to be of type SceneTransitionControlAsset!");
        }

        void IOnPostSceneEnter.OnPostEnter()
        {
            var transitionControl = m_asset as SceneTransitionControlAsset;
            StartCoroutine(ChangeSceneAfterDelay(transitionControl.TransitionTime, transitionControl.NextSceneName));
        }

        private IEnumerator ChangeSceneAfterDelay(float transitionTime, string nextSceneName)
        {
            yield return new WaitForSeconds(transitionTime);
            m_application.ChangeState(nextSceneName);
        }

        bool IOnPostSceneEnter.ShouldBlockEnter()
        {
            return false;
        }
    }
}