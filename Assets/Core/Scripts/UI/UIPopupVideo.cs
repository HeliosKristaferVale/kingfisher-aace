﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Kingfisher.Scenes.Events;

namespace Kingfisher.UI
{
    public class UIPopupVideo : UIPopupImage
    {
        [SerializeField] private RawImage m_displayVideo;

        [Tooltip("If true, the UI element for the display video will have SetNativeSize called on it after the video loads.")]
        [SerializeField] private bool m_videoIsNativeSize = true;

        private KingfisherMediaPlayer m_mediaPlayerComponent;

        protected override void Awake()
        {
            base.Awake();

            m_mediaPlayerComponent = UIPopupFactory.CreateVideoPopup(gameObject); // gameObject.AddComponent<KingfisherMediaPlayer>();
            m_mediaPlayerComponent.OnPlaybackTextureChanged += OnMediaTextureLoaded;
            m_mediaPlayerComponent.OnVideoPlaybackFinished += OnPlayFinished;
            m_displayVideo.DOFade(0f, 0.001f);
        }

        public override void PopUp(bool skipLightbox = false)
        {
            base.PopUp(skipLightbox);

            var videoAsset = m_asset as PopupVideoAsset;
            m_mediaPlayerComponent.LoadMediaAsync(videoAsset.VideoSettings.MediaPath, OnMediaPlayerReadyToPlay);

            if(videoAsset.DuckingSettings.MuteSceneAudio)
            {
                // Walk up to our transform and then get all components in children, kinda awful but hopefully good enough for most cases.
                // ToDo: Replace this with a "Get SceneBase" type call so we handle nestled hierarchies better 
                Transform parent = transform.parent;
                if(parent)
                {
                    foreach(var audioSource in parent.GetComponentsInChildren<UISceneAudio>())
                    {
                        audioSource.Stop();
                    }
                }
            }

            if(videoAsset.DuckingSettings.DuckBackgroundAudio)
            {
                var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                if (sceneTransitionEvent != null)
                {
                    sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(true, videoAsset.DuckingSettings.DuckAmountInDb));
                }
            }
        }

        public override void PopDown(bool skipLightbox = false)
        {
            var videoAsset = m_asset as PopupVideoAsset;
            if (videoAsset.DuckingSettings.UnduckBackgroundAudio)
            {
                var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                if (sceneTransitionEvent != null)
                {
                    sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(false, videoAsset.DuckingSettings.DuckAmountInDb));
                }
            }

            base.PopDown(skipLightbox);

            // Fade out the video as we pop down
            m_displayVideo.DOKill();
            m_displayVideo.DOFade(0f, m_scaleSpeed*2f).SetEase(Ease.InCirc).SetSpeedBased().OnComplete(() =>
           {
                m_mediaPlayerComponent.UnloadMedia();
           });
        }
        
        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            var videoAsset = asset as PopupVideoAsset;
            if (videoAsset == null)
                throw new ArgumentException("asset is expected to be of type PopupVideoAsset");

            m_mediaPlayerComponent.SetLoops(videoAsset.VideoSettings.Loop);
        }

        private void OnMediaTextureLoaded(Texture texture, bool requiresYFlip)
        {
            Debug.LogFormat("OnMediaTextureLoaded. Texture: {0} requiresYFlip: {1}", texture ? string.Format("{0}:{1}x{2}", texture.name, texture.width, texture.height) : "NULL", requiresYFlip);
            // Debug.Break();
            
            m_displayVideo.texture = texture;
            if(m_videoIsNativeSize)
                m_displayVideo.SetNativeSize();            
            m_displayVideo.rectTransform.localScale = new Vector3(1f, requiresYFlip ? -1f : 1f, 1f);
        }

        private void OnPlayFinished()
        {
            var videoAsset = m_asset as PopupVideoAsset;
            if (videoAsset.ClosePopupOnFinish)
                PopDown();
        }

        private void OnMediaPlayerReadyToPlay()
        {
            var videoAsset = m_asset as PopupVideoAsset;
            if (videoAsset.VideoSettings.AutoPlay)
            {
                m_mediaPlayerComponent.Play();

                // Fade in the video now that it's available.
                m_displayVideo.DOKill();
                m_displayVideo.DOFade(1f, m_scaleSpeed).SetEase(Ease.OutCirc).SetSpeedBased();
            }
        }
    }
}