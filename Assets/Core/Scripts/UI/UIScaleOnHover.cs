﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

namespace Kingfisher.UI
{
    public class UIScaleOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private float m_finalScale = 1.1f;
        [SerializeField] private Transform m_scaleTransform;

        private Transform m_transformCache;
        private Selectable m_interactiveComponent;

        private void Awake()
        {
            m_transformCache = m_scaleTransform != null ? m_scaleTransform : GetComponent<Transform>();
            m_interactiveComponent = GetComponent<Selectable>();
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            // Debug.Log("PointerEnter");

            // If this is applied to a interactable object, don't hover if it's non-interactable.
            bool bEnabled = true;
            if (m_interactiveComponent != null)
                bEnabled = m_interactiveComponent.interactable;

            if(bEnabled)
            {
                //float newScale = m_finalScale - 1f;
                //m_transformCache.DOScale(newScale, 1f).SetSpeedBased().SetRelative();
                m_transformCache.DOKill();
                m_transformCache.DOScale(m_finalScale, 1f).SetSpeedBased();
            }
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            // Debug.Log("PointerExit");

            // If this is applied to a interactable object, don't hover if it's non-interactable.
            bool bEnabled = true;
            if (m_interactiveComponent != null)
                bEnabled = m_interactiveComponent.interactable;

            // if (bEnabled && !DOTween.IsTweening(m_transformCache))
            {
                //float curScale = m_transformCache.localScale.x;
                //float newScale = 1f - m_finalScale;
                //float newScale = 1f - (1f / curScale);
                //m_transformCache.DOScale(-newScale, 1f).SetSpeedBased().SetRelative();
                m_transformCache.DOKill();
                m_transformCache.DOScale(1f, 1f).SetSpeedBased();
            }
        }
    }
}
