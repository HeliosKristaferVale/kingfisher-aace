﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Kingfisher.UI
{
    public class UIPopupImage : UIBasePopup
    {
        [SerializeField] private Image m_displayImage;
        protected float m_scaleSpeed = 3.5f;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            // Reset state
            m_displayImage.gameObject.SetActive(false);
            m_displayImage.transform.localScale = Vector3.one * 0.25f;
        }

        public override void PopUp(bool skipLightbox = false)
        {
            base.PopUp();

            m_displayImage.transform.DOKill();
            m_displayImage.DOFade(1f, m_scaleSpeed).SetEase(Ease.OutCirc).SetSpeedBased().SetDelay(0.25f);
            m_displayImage.transform.DOScale(1f, m_scaleSpeed).SetSpeedBased().SetDelay(0.25f).OnStart(() =>
           {
               m_displayImage.gameObject.SetActive(true);
           });
        }

        public override void PopDown(bool skipLightbox = false)
        {
            SetLightboxEnabled(false);

            m_displayImage.transform.DOKill();
            m_displayImage.DOFade(0f, m_scaleSpeed).SetEase(Ease.InCirc).SetSpeedBased();
            m_displayImage.transform.DOScale(0.25f, m_scaleSpeed).SetEase(Ease.InCirc).SetSpeedBased().OnComplete(() =>
           {
               m_displayImage.gameObject.SetActive(false);
           });

            base.PopDown(true);
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            var imageAsset = asset as PopupImageAsset;
            if (imageAsset == null)
                throw new ArgumentException("asset is expected to be of type PopupImageAsset");

            m_displayImage.sprite = imageAsset.Image;
            m_displayImage.SetNativeSize();
        }
    }
}