﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using DG.Tweening;
using Kingfisher.Scenes.Events;
using System.Collections;

namespace Kingfisher.UI
{
    public class UISceneAudio : UIBasePrefab, IOnPostSceneEnter, IOnPreSceneExit
    {
        [SerializeField] private AudioSource m_audioSource;

        private IEnumerator m_audioClipFinishedHandle;

        protected virtual void Awake()
        {
            m_audioSource.playOnAwake = false;
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            var audioAsset = asset as SceneAudioAsset;
            if (audioAsset == null)
                throw new ArgumentException("asset is expected to be of type SceneAudioAsset");

            m_audioSource.clip = audioAsset.AudioClip;
            if (audioAsset.DuckingSettings.MuteSceneAudio)
                throw new System.NotSupportedException("UISceneAudio doesn't support muting other SceneAudio at the start right now.");
        }

        /// <summary>
        /// Call this to softly fade in the audio while respecting the delay specified by the asset.
        /// </summary>
        public void Play()
        {
            SceneAudioAsset saa = m_asset as SceneAudioAsset;
            m_audioSource.DOFade(1f, 2f).SetDelay(saa.Delay).SetSpeedBased().OnStart(() =>
            {
                if (saa.DuckingSettings.DuckBackgroundAudio)
                {
                    var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                    if (sceneTransitionEvent != null)
                    {
                        sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(true, saa.DuckingSettings.DuckAmountInDb));
                    }
                }

                if (m_audioClipFinishedHandle != null)
                    StopCoroutine(m_audioClipFinishedHandle);
                m_audioClipFinishedHandle = StopSourceAtEndOfClip(saa.AudioClip.length);
                StartCoroutine(m_audioClipFinishedHandle);

                m_audioSource.Play();
            });
        }

        /// <summary>
        /// Call this to softly fade out the audio and stop it when finished.
        /// </summary>
        public void Stop()
        {
            if (m_audioClipFinishedHandle != null)
                StopCoroutine(m_audioClipFinishedHandle);
            m_audioClipFinishedHandle = null;

            SceneAudioAsset saa = m_asset as SceneAudioAsset;
            m_audioSource.DOFade(0f, 2f).SetSpeedBased().OnComplete(() =>
           {
               if (saa.DuckingSettings.UnduckBackgroundAudio)
               {
                   var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                   if (sceneTransitionEvent != null)
                   {
                       sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(false, saa.DuckingSettings.DuckAmountInDb));
                   }
               }

               m_audioSource.Stop();
           });
        }

        private IEnumerator StopSourceAtEndOfClip(float clipLengthSeconds)
        {
            yield return new WaitForSeconds(clipLengthSeconds);

            Stop();
        }
        #region Interface Implementations
        void IOnPostSceneEnter.OnPostEnter()
        {
            // Pop our audio back to full in case they want it to start immediately without a fade.
            // m_hasFiredAudioFinishedEvent = false;
            m_audioSource.volume = 1f;

            // Check to see if we need to disable our button because they have already visited the specified scenes or not.
            SceneAudioAsset saa = m_asset as SceneAudioAsset;
            bool bEnabled = true;
            if (saa.DisableIfVisitedAllScenes.Count > 0)
            {
                bEnabled = false;
                for (int i = 0; i < saa.DisableIfVisitedAllScenes.Count; i++)
                {
                    if (!m_application.HasSceneBeenVisited(saa.DisableIfVisitedAllScenes[i].SceneName))
                        bEnabled = true;
                }
            }

            if (bEnabled)
                Play();
        }

        bool IOnPostSceneEnter.ShouldBlockEnter()
        {
            return false;
        }

        void IOnPreSceneExit.OnPreExit()
        {
            // Ensure we softly fade out as the scene ends if they exit the scene before the audio clip finishes.
            Stop();
        }

        bool IOnPreSceneExit.ShouldBlockExit()
        {
            return false;
        }
        #endregion
    }
}