﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine.UI;

namespace Kingfisher.UI
{
    /// <summary>
    /// This is likely the class you will want to inherit from for most in-vr assets, <see cref="UIBasePrefab"/> is provided
    /// in the event that you do not want some of these fairly common functionalities.
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class UIBaseScenePrefab : UIBasePrefab, IOnPostSceneEnter, IOnPreSceneExit
    {
        /// <summary> What content do you want to have animated in/out for create/destroy? </summary>
        [Tooltip("This is the gameobject that Create/Destroy popup animates. Can't be self as it also toggles GameObject activeness!")]
        [SerializeField] protected Transform m_scaledContent;

        protected float m_creationDelay = 0f;
        protected CanvasGroup m_canvasGroup;

        /// <summary> Scene elements can be created with an additional delay, store the handle 
        /// to the coroutine that starts that so we can cancel it if we exit too early. </summary>
        protected IEnumerator m_delayedCreateHandle;

        protected virtual void Awake()
        {
            m_canvasGroup = GetComponent<CanvasGroup>();
        }

        protected virtual void OnEnable()
        {
            m_scaledContent.localScale = Vector3.one * 0.25f;
            m_scaledContent.gameObject.SetActive(false);
        }

        protected virtual void OnDisable()
        {
            if (m_delayedCreateHandle != null)
                StopCoroutine(m_delayedCreateHandle);
        } 

        /// <summary>
        /// This is called to create the scene element from nothing. This plays the create animation,
        /// but should not be called out of sequence. Don't enable any user interactivity until
        /// the animation has finished. 
        /// </summary>
        protected virtual void CreatePopup()
        {
            OnCreatePopupStarted();

            m_canvasGroup.alpha = 0f;
            m_scaledContent.gameObject.SetActive(true);

            m_scaledContent.DOKill();
            // m_scaledContent.DOPunchRotation(new Vector3(0, 0, 3f), 0.45f);
            m_canvasGroup.DOFade(1f, 2f).SetEase(Ease.InCirc).SetSpeedBased();
            m_scaledContent.DOScale(1f, 1.5f).SetEase(Ease.OutBack).SetSpeedBased().OnComplete(() =>
            {
                OnCreatePopupFinished();
            });
        }

        /// <summary> Called when the CreatePopup animations start. Use it to do quick loading or state reset if needed. </summary>
        protected virtual void OnCreatePopupStarted() { }

        /// <summary> Called when the CreatePopup animations finish. Use to re-enable ui interactions, etc. </summary>
        protected virtual void OnCreatePopupFinished() { }

        /// <summary>
        /// This is called to send the popup back to nothing. This plays the destruction/hide animation,
        /// but does not actually destroy the gameobject. You should disable user interactivity immediately
        /// </summary>
        protected virtual void DestroyPopup()
        {
            m_scaledContent.DOKill();
            // m_scaledContent.DOPunchRotation(new Vector3(0, 0, 3f), 0.35f);
            m_canvasGroup.DOFade(0f, 2f).SetEase(Ease.InCirc).SetSpeedBased();
            m_scaledContent.DOScale(0.25f, 1.5f).SetSpeedBased().SetEase(Ease.InCirc).OnComplete(() => OnDestroyPopupFinished());
        }

        /// <summary> Called when the CreatePopup animations finish. Use to re-enable ui interactions, etc. </summary>
        protected virtual void OnDestroyPopupFinished()
        {
            m_scaledContent.gameObject.SetActive(false);
        }

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                CreatePopup();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                DestroyPopup();
            }
        }

        /// <summary>
        /// This can be called by other scripts if they have done something that modifies the position
        /// of the prefab in the scene. You should implement this and update your object using specific
        /// implementation logic.
        /// </summary>
        public virtual void UpdateSceneTransform()
        {
        }

        /// <summary>
        /// This converts the UV Coordinates of a location into a worldspace direction.
        /// This direction can then be used to make the object appear over that location.
        /// </summary>
        /// <param name="uv">
        /// Should be in [0-1] Range and relative to the Top Left of Equirectangular Footage.
        /// </param>
        /// <returns>Direction to that point in World Space.</returns>
        protected virtual Vector3 ConvertUVCoordToDirection(Vector2 uv)
        {
            // If they're at the very top or very bottom nudge them off a bit so they can face the player.
            if (uv.y == 0f)
                uv.y += 0.01f;
            if (uv.y == 1f)
                uv.y -= 0.01f;

            Quaternion rot = Quaternion.identity;
            float y = uv.y - 0.5f; // Remap [0,1] to [-.5, .5]

            rot *= Quaternion.AngleAxis((uv.x * (Mathf.PI * 2) + (Mathf.PI / 2)) * Mathf.Rad2Deg, Vector3.up);
            rot *= Quaternion.AngleAxis((y * Mathf.PI) * Mathf.Rad2Deg, Vector3.right);
            return rot * Vector3.forward;
        }

        /// <summary>
        /// Rotate the specified transform to face the user's VR camera. Rotating an element to face the camera
        /// should cause the content to appear to be correctly placed in 2D space around them.
        /// </summary>
        /// <param name="inTransform"></param>
        protected virtual void RotateTransformToFaceCamera(Transform inTransform, bool bRotateHorizontal, bool bRotateVertical)
        {
            Vector3 dirToCamera = inTransform.position;

            // If they don't want it to rotate horizontally to face the camera, skip that
            if (!bRotateHorizontal)
            {
                dirToCamera.x = 0;
                dirToCamera.y = 0;
            }

            // If they don't want to rotate vertically to face the camera, skip that too
            if (!bRotateVertical)
            {
                dirToCamera.y = 0;
            }

            dirToCamera.Normalize();
            inTransform.rotation = Quaternion.LookRotation(dirToCamera, Vector3.up);
        }

        protected virtual IEnumerator CreatePopupAfterDelay()
        {
            yield return new WaitForSeconds(m_creationDelay);
            CreatePopup();
            m_delayedCreateHandle = null;
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);
            BaseTransformedAsset tAsset = m_asset as BaseTransformedAsset;
            if(tAsset != null)
            {
                m_creationDelay = tAsset.ShowDelay;
            }
        }


        #region Interface Implementations
        void IOnPostSceneEnter.OnPostEnter()
        {
            m_delayedCreateHandle = CreatePopupAfterDelay();
            StartCoroutine(m_delayedCreateHandle);
        }

        bool IOnPostSceneEnter.ShouldBlockEnter()
        {
            // Popups just need to wait until after the transition color finishes before display
            // so they have no reason to block.
            return false;
        }

        void IOnPreSceneExit.OnPreExit()
        {
            if(m_delayedCreateHandle == null)
            {
                DestroyPopup();
            }
            else
            {
                StopCoroutine(m_delayedCreateHandle);
                m_delayedCreateHandle = null;
            }
        }

        bool IOnPreSceneExit.ShouldBlockExit()
        {
            // This is kind of dodgy, but it makes it so we don't leave the scene until we finish destroying popups
            return m_scaledContent.gameObject.activeSelf;
        }
        #endregion
    }
}