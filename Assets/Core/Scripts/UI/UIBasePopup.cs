﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System.Collections;

namespace Kingfisher.UI
{
    public abstract class UIBasePopup : UIBaseScenePrefab
    {
        public bool IsOpen { get { return m_isPoppedUp; } }

        [SerializeField] private Button m_iconButton;
        [SerializeField] private Button m_closeButton;

        private bool m_closeOtherPopupsOnOpen;
        private bool m_enableLightboxOnOpen;

        // Set as soon as soon as it starts to pop up and unset when it finishes pop downing.
        private bool m_isPoppedUp;
        private List<UIBasePopup> m_adjacentPopups;

        protected override void Awake()
        {
            base.Awake();
            
            // Register Button Listeners
            m_iconButton.onClick.AddListener(OnIconButtonClicked);
            m_closeButton.onClick.AddListener(OnCloseButtonClicked);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            // Reset our state here
            m_closeButton.transform.localScale = Vector3.one * 0.25f;
            m_closeButton.gameObject.SetActive(false);

            m_canvasGroup.interactable = false;
        }

        protected override void DestroyPopup()
        {
            m_canvasGroup.interactable = false;
            base.DestroyPopup();
        }

        protected override void OnCreatePopupFinished()
        {
            base.OnCreatePopupFinished();
            m_canvasGroup.interactable = true;
        }

        protected virtual void OnIconButtonClicked()
        {
            PopUp();
        }

        protected virtual void OnCloseButtonClicked()
        {
            PopDown();
        }

        /// <summary>
        /// Open this popup marker and display 
        /// </summary>
        /// <param name="skipLightbox"></param>
        public virtual void PopUp(bool skipLightbox = false)
        {
            Debug.LogFormat("{0} PopUp", name);
            if(m_isPoppedUp)
            {
                Debug.LogWarningFormat("Popup tried to open while still open. Early out-ing, issues? Popup: {0}", name);
                return;
            }

            // Close other Popups before we open ours so we override any external state changes.
            if (m_closeOtherPopupsOnOpen)
                CloseOtherPopups();

            m_isPoppedUp = true;

            // Scale down the Popup Icon and then hide
            m_iconButton.transform.DOKill();
            m_iconButton.GetComponent<Graphic>().DOFade(0f, 3f).SetEase(Ease.InCirc).SetSpeedBased();
            m_iconButton.transform.DOScale(0.25f, 3f).SetEase(Ease.InCirc).SetSpeedBased().OnComplete(() =>
            {
                m_iconButton.gameObject.SetActive(false);
            });

            // Scale up the Close button
            m_closeButton.transform.DOKill();
            m_closeButton.transform.DOScale(1f, 3f).SetEase(Ease.OutCirc).SetSpeedBased().OnStart(() =>
            {
                m_closeButton.gameObject.SetActive(true);
            }).SetDelay(1.5f);

            // Background Lightbox
            if (!skipLightbox)
                SetLightboxEnabled(m_enableLightboxOnOpen);
        }

        public virtual void PopDown(bool skipLightbox = false)
        {
            Debug.LogFormat("{0} PopDown", name);

            // Scale up the Popup Icon 
            m_iconButton.gameObject.SetActive(true);
            m_iconButton.transform.DOKill();
            m_iconButton.GetComponent<Graphic>().DOFade(1f, 3f).SetEase(Ease.OutCirc).SetSpeedBased();
            m_iconButton.transform.DOScale(1f, 3f).SetEase(Ease.OutCirc).SetSpeedBased();

            // Scale down the Close button and then hide
            m_closeButton.transform.DOKill();
            m_closeButton.transform.DOScale(0.25f, 3f).SetEase(Ease.InCirc).SetSpeedBased().OnComplete(() =>
            {
                m_closeButton.gameObject.SetActive(false);
                m_isPoppedUp = false;
            });

            if (!skipLightbox)
                SetLightboxEnabled(false);
        }

        protected void SetLightboxEnabled(bool bEnabled)
        {
            m_application.MediaSphere.SetLightboxMode(bEnabled);
        }

        protected void CloseOtherPopups()
        {
            if(m_adjacentPopups == null)
            {
                m_adjacentPopups = new List<UIBasePopup>(transform.parent.GetComponentsInChildren<UIBasePopup>(true));
                m_adjacentPopups.Remove(this);
            }

            for(int i = 0; i < m_adjacentPopups.Count; i++)
            {
                if (m_adjacentPopups[i].IsOpen)
                    m_adjacentPopups[i].PopDown();
            }
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            var popupAsset = asset as BasePopupAsset;
            if (popupAsset == null)
                throw new ArgumentException("asset is expected to be of type BasePopupAsset");

            SetupButtonAnimations(m_iconButton, popupAsset.IconNormalState, popupAsset.IconPressedState, popupAsset.IconHighlightState, popupAsset.IconDisabledState);
            SetupButtonAnimations(m_closeButton, popupAsset.CloseNormalState, popupAsset.ClosePressedState, popupAsset.CloseHighlightState, popupAsset.CloseDisabledState);
            m_closeOtherPopupsOnOpen = popupAsset.CloseOtherPopupsOnOpen;
            m_enableLightboxOnOpen = popupAsset.EnableLightboxOnOpen;

            Vector3 spawnPos = ConvertUVCoordToDirection(popupAsset.Position) * popupAsset.Distance;
            transform.position = spawnPos;
            RotateTransformToFaceCamera(transform, popupAsset.FaceCameraHorizontally, popupAsset.FaceCameraVertically);
        }

        protected virtual void SetupButtonAnimations(Button button, Sprite normalState, Sprite pressedState, Sprite highlightState, Sprite disabledState)
        {
            button.transition = Selectable.Transition.SpriteSwap;

            var btnGraphic = button.targetGraphic as Image;
            if (btnGraphic == null)
                throw new ArgumentException("button expected to have target graphic for display");

            btnGraphic.sprite = normalState;
            btnGraphic.SetNativeSize();

            SpriteState spriteState = new SpriteState();
            spriteState.pressedSprite = pressedState;
            spriteState.highlightedSprite = highlightState;
            spriteState.disabledSprite = disabledState;
            button.spriteState = spriteState;
        }
    }
}