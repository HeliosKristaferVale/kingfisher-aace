﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;

namespace Kingfisher.UI
{
    /// <summary>
    /// This spawns another asset and makes that asset a child of this asset. Then, every frame,
    /// it measures the distance of that asset from where the camera is looking. If it's moved too
    /// far away, then it scoots closer. This allows you to make a "soft" locked asset - it follows the
    /// player's view but allows some tolerance to look around.
    /// 
    /// Limitations: Follow Vertically doesn't really work that well. If you're going to plan on using that
    /// you get to use fix the edge cases of looking straight up and straight down.
    /// </summary>
    public class UIFollowView : UIBasePrefab
    {
        private Transform m_content;
        private bool m_followHorizontally;
        private bool m_followVertically;
        private float m_tetherFoV;

        private void Update()
        {
            Transform camTransform = m_application.HMD.transform;
            Vector3 dirToContent = (m_content.position - camTransform.position).normalized;
            Vector3 contentAbsRot = Quaternion.LookRotation(dirToContent, Vector3.up).eulerAngles;

            bool bRotated = false;
            if(m_followHorizontally)
            {
                float offset = Mathf.DeltaAngle(camTransform.eulerAngles.y, contentAbsRot.y);
                if(Mathf.Abs(offset) > m_tetherFoV)
                {
                    float counterRot = (Mathf.Abs(offset) - m_tetherFoV) * -Mathf.Sign(offset);
                    m_content.RotateAround(camTransform.position, Vector3.up, counterRot);
                    bRotated = true;
                }
            }
            if (m_followVertically)
            {
                float offset = Mathf.DeltaAngle(camTransform.eulerAngles.x, contentAbsRot.x);
                if (Mathf.Abs(offset) > m_tetherFoV)
                {
                    float counterRot = (Mathf.Abs(offset) - m_tetherFoV) * -Mathf.Sign(offset);
                    m_content.RotateAround(camTransform.position, camTransform.right, counterRot);
                    bRotated = true;
                }
            }

            if(bRotated)
            {
                // Check to see if we can update the content's facing direction ToDo: Don't GetComponent every frame.
                UIBaseScenePrefab sceneUI = m_content.gameObject.GetComponent<UIBaseScenePrefab>();
                sceneUI.UpdateSceneTransform();
            }
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            // Need to get the FollowView asset out which requires us to spawn whatever asset it wraps, no recusion supported. 
            FollowViewAsset viewAsset = asset as FollowViewAsset;
            if(viewAsset == null)
                throw new ArgumentException("asset is expected to be of type FollowViewAsset");

            BaseAsset childAsset = viewAsset.FollowingAsset;
            if (childAsset == null)
                throw new ArgumentException("FollowingAsset is null, don't have nothing follow your view!");

            // Instantiate our asset and let it load itself from the asset
            UIBasePrefab childPrefab = Instantiate(childAsset.Prefab);
            childPrefab.transform.SetParent(transform);
            childPrefab.LoadFromAsset(childAsset);

            // Now that we've loaded our from asset, it should do any positioning logic it desires.
            // That effectively determines our start x,y so that if you only follow view on one direction
            // then it's at the correct location. We assume the prefab is the object that is supposed to follow...
            m_content = childPrefab.transform;

            m_tetherFoV = viewAsset.MaximumFieldOfView;
            m_followHorizontally = viewAsset.FollowHorizontally;
            m_followVertically = viewAsset.FollowVertically;
        }
    }
}
