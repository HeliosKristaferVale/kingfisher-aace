﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;

namespace Kingfisher.UI
{
    /// <summary>
    /// This is an abstract baseclass that "UI" classes (anything displayed in the scene really) use
    /// to support spawning almost anything in the scene. Scenes take an array of <see cref="ScriptableObjects.UI.BaseAsset"/>
    /// which themselves contain a <see cref="UIBasePrefab"/> to spawn in-scene.
    /// </summary>
    public abstract class UIBasePrefab : MonoBehaviour, IStateMachineReciever
    {
        protected KingfisherApplication m_application;
        protected BaseAsset m_asset;

        /// <summary>
        /// An instance of the UIBasePrefab will be instantiated based on the UIBasePrefab specified by a given 
        /// <see cref="BaseAsset"/>. Then the BaseAsset will be provided to the UIBasePrefab via this function.
        /// This allows you to cast the BaseAsset to a specific derived typd retrieve the data from it.
        /// </summary>
        /// <param name="asset">The asset that was responsible for spawning this UIBasePrefab. </param>
        public virtual void LoadFromAsset(BaseAsset asset)
        {
            gameObject.name = asset.name + "_Prefab";
            m_asset = asset;
        }

        public virtual void SetStateMachine(KingfisherApplication machine)
        {
            m_application = machine;
        }
    }
}