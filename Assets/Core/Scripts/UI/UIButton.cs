﻿using System;
using System.Collections;
using Kingfisher.ScriptableObjects.UI;
using Kingfisher.Scenes.Events;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Kingfisher.ScriptableObjects.Scenes;

namespace Kingfisher.UI
{
    public class UIButton : UIBaseScenePrefab
    {
        [SerializeField] private Button m_displayButton;
        [SerializeField] private Image m_visitedImage;
        [SerializeField] private float m_buttonDelay = 0.15f;

        private string m_interactionAction;
        private List<BaseSceneAsset> m_sceneList;

        private bool m_hasBeenPressed;
        IEnumerator m_transitionCoroutine;
        private bool m_faceCameraHorizontally;
        private bool m_faceCameraVertically;
        private ButtonAsset m_buttonAsset;

        protected override void Awake()
        {
            base.Awake();

            m_visitedImage.gameObject.SetActive(false);
            m_displayButton.onClick.AddListener(OnButtonClicked);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            m_hasBeenPressed = false;

            // We want to call this each time you visit a scene with this button, but OnEnable is called before LoadFromAsset and SetStateMachine
            if (m_sceneList != null)
            {
                UpdateButtonInteractable();
                UpdateButtonVisited();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_transitionCoroutine != null)
                StopCoroutine(m_transitionCoroutine);
        }

        private void UpdateButtonInteractable()
        {
            // Check to see if we need to disable our button because they have already visited the specified scenes or not.
            bool bEnabled = true;
            if (m_sceneList.Count > 0)
            {
                bEnabled = false;
                for (int i = 0; i < m_sceneList.Count; i++)
                {
                    if (!m_application.HasSceneBeenVisited(m_sceneList[i].SceneName))
                        bEnabled = true;
                }
            }

            m_displayButton.interactable = bEnabled;
        }

        private void UpdateButtonVisited()
        {
            // Check to see if we should show our button's "Visited" mark. This is an optional sprite, so if they don't
            // specify then we just won't show it.
            ButtonAsset buttonAsset = m_asset as ButtonAsset;
            if(buttonAsset.VisitedSprite != null)
            {
                m_visitedImage.sprite = buttonAsset.VisitedSprite;
                m_visitedImage.SetNativeSize();

                bool bEnabled = false;
                if (buttonAsset.MarkVisitedIfAllScenes.Count > 0)
                {
                    bEnabled = true;
                    for (int i = 0; i < buttonAsset.MarkVisitedIfAllScenes.Count; i++)
                    {
                        if (!m_application.HasSceneBeenVisited(buttonAsset.MarkVisitedIfAllScenes[i].SceneName))
                            bEnabled = false;
                    }
                }

                m_visitedImage.gameObject.SetActive(bEnabled);
            }
        }

        private void OnButtonClicked()
        {
            if (m_hasBeenPressed)
                return;

            m_hasBeenPressed = true;
            m_transitionCoroutine = TransitionAfterDelay(m_buttonDelay);
            StartCoroutine(m_transitionCoroutine);
        }

        private IEnumerator TransitionAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);

            switch (m_buttonAsset.ButtonAction)
            {
                case ButtonAsset.Action.TransitionScene:
                    var sceneTransitionEvent = GetComponentInParent<ISceneMessage<SceneTransitionEvent>>();
                    if (sceneTransitionEvent != null)
                    {
                        sceneTransitionEvent.HandleSceneEvent(new SceneTransitionEvent(m_interactionAction)); ;
                    }
                    break;
                case ButtonAsset.Action.StartBackgroundVideo:
                    var startVideoEvent = GetComponentInParent<ISceneMessage<BackgroundVideoEvent>>();
                    if (startVideoEvent != null)
                    {
                        startVideoEvent.HandleSceneEvent(new BackgroundVideoEvent(true));
                    }
                    break;
                default:
                    throw new System.NotSupportedException(string.Format("Unsupported Button Action: {0}", m_buttonAsset.ButtonAction));
            }

            if (m_buttonAsset.HideButtonAfterUse)
                DestroyPopup();
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            m_buttonAsset = asset as ButtonAsset;
            if (m_buttonAsset == null)
                throw new ArgumentException("asset is expected to be of type ButtonAsset");

            m_displayButton.transition = Selectable.Transition.SpriteSwap;
            m_interactionAction = m_buttonAsset.Interaction;
            m_sceneList = m_buttonAsset.DisableIfVisitedAllScenes;

            var btnGraphic = m_displayButton.targetGraphic as Image;
            if (btnGraphic == null)
                throw new ArgumentException("button expected to have target graphic for display");

            btnGraphic.sprite = m_buttonAsset.NormalState;
            btnGraphic.SetNativeSize(); 

            SpriteState spriteState = new SpriteState();
            spriteState.pressedSprite = m_buttonAsset.PressedState;
            spriteState.highlightedSprite = m_buttonAsset.HighlightState;
            spriteState.disabledSprite = m_buttonAsset.DisabledState;
            m_displayButton.spriteState = spriteState;

            Vector3 spawnPos = ConvertUVCoordToDirection(m_buttonAsset.Position) * m_buttonAsset.Distance;
            transform.position = spawnPos;

            m_faceCameraHorizontally = m_buttonAsset.FaceCameraHorizontally;
            m_faceCameraVertically = m_buttonAsset.FaceCameraVertically;

            UpdateSceneTransform();
        }

        public override void UpdateSceneTransform()
        {
            base.UpdateSceneTransform();

            RotateTransformToFaceCamera(transform, m_faceCameraHorizontally, m_faceCameraVertically);
        }

        public override void SetStateMachine(KingfisherApplication machine)
        {
            base.SetStateMachine(machine);

            UpdateButtonInteractable();
            UpdateButtonVisited();
        }
    }
}