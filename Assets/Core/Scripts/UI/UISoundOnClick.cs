﻿using UnityEngine;
using UnityEngine.UI;

namespace Kingfisher.UI
{
    [RequireComponent(typeof(Button))]
    public class UISoundOnClick : MonoBehaviour
    {
        [SerializeField] private AudioClip m_clickSound;

        private AudioSource m_audioSource;
        private Button m_button;

        private void Awake()
        {
            m_audioSource = gameObject.AddComponent<AudioSource>();
            m_audioSource.clip = m_clickSound;
            m_audioSource.spatialBlend = 0f;
            m_audioSource.loop = false;

            m_button = GetComponent<Button>();
            m_button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            m_audioSource.Play();
        }
    }
}
