﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher.UI
{
    public static class UIPopupFactory
    {
        public static KingfisherMediaPlayer CreateVideoPopup(GameObject targetGameObject)
        {
#if AVPRO
            targetGameObject.AddComponent<KingfisherAvProMediaPlayer>();
#endif

#if UNITYVIDEOPLAYER
            targetGameObject.AddComponent<KingfisherUnityVideoPlayerMediaPlayer>();
#endif
            return targetGameObject.GetComponent<KingfisherMediaPlayer>();
        }
    }


}
