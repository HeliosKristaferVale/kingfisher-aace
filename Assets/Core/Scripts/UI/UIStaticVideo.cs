﻿using System;
using Kingfisher.ScriptableObjects.UI;
using UnityEngine;
using UnityEngine.UI;
using Kingfisher.Scenes.Events;

namespace Kingfisher.UI
{
    public class UIStaticVideo : UIBaseScenePrefab
    {
        [SerializeField] private RawImage m_displayVideo;

        protected bool m_faceCameraHorizontally;
        protected bool m_faceCameraVertically;

        [Tooltip("If true, the UI element for the display video will have SetNativeSize called on it after the video loads.")]
        [SerializeField] protected bool m_videoIsNativeSize = true;

        protected KingfisherMediaPlayer m_mediaPlayerComponent;

        protected override void Awake()
        {
            base.Awake();

            m_mediaPlayerComponent = UIPopupFactory.CreateVideoPopup(gameObject); //gameObject.AddComponent<KingfisherMediaPlayer>();
            m_mediaPlayerComponent.OnPlaybackTextureChanged += OnMediaTextureLoaded;
            m_mediaPlayerComponent.OnVideoPlaybackFinished += OnPlayFinished;
        }

        protected override void CreatePopup()
        {
            base.CreatePopup();
        }

        protected override void OnCreatePopupStarted()
        {
            base.OnCreatePopupStarted();

            var videoAsset = m_asset as StaticVideoAsset;
            m_mediaPlayerComponent.LoadMediaAsync(videoAsset.VideoSettings.MediaPath, OnMediaPlayerReadyToPlay);
        }

        protected override void DestroyPopup()
        {
            var videoAsset = m_asset as StaticVideoAsset;
            if (videoAsset.DuckingSettings.UnduckBackgroundAudio)
            {
                var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                if (sceneTransitionEvent != null)
                {
                    sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(false, videoAsset.DuckingSettings.DuckAmountInDb));
                }
            }

            base.DestroyPopup();
        }

        protected override void OnDestroyPopupFinished()
        {
            base.OnDestroyPopupFinished();

            m_mediaPlayerComponent.UnloadMedia();
        }

        public override void LoadFromAsset(BaseAsset asset)
        {
            base.LoadFromAsset(asset);

            var videoAsset = asset as StaticVideoAsset;
            if (videoAsset == null)
                throw new ArgumentException("asset is expected to be of type VideoAsset");

            m_mediaPlayerComponent.SetLoops(videoAsset.VideoSettings.Loop);

            Vector3 spawnPos = ConvertUVCoordToDirection(videoAsset.Position) * videoAsset.Distance;
            transform.position = spawnPos;

            m_faceCameraHorizontally = videoAsset.FaceCameraHorizontally;
            m_faceCameraVertically = videoAsset.FaceCameraVertically;

            UpdateSceneTransform();
        }

        protected virtual void OnMediaTextureLoaded(Texture texture, bool requiresYFlip)
        {
            Debug.LogFormat("OnMediaTextureLoaded: {0}", texture);
            // Debug.LogWarningFormat("{0} RequiresY Flip: {1}", name, requiresYFlip);
            m_displayVideo.texture = texture;
            if (m_videoIsNativeSize)
                m_displayVideo.SetNativeSize();
            m_displayVideo.rectTransform.localScale = new Vector3(1f, requiresYFlip ? -1f : 1f, 1f);
        }

        private void OnPlayFinished()
        {
        }

        protected virtual void OnMediaPlayerReadyToPlay()
        {
            var videoAsset = m_asset as StaticVideoAsset;
            if (videoAsset.VideoSettings.AutoPlay)
            {
                m_mediaPlayerComponent.Play();

                if (videoAsset.DuckingSettings.MuteSceneAudio)
                {
                    // Walk up to our transform and then get all components in children, kinda awful but hopefully good enough for most cases.
                    // ToDo: Replace this with a "Get SceneBase" type call so we handle nestled hierarchies better 
                    Transform parent = transform.parent;
                    if (parent)
                    {
                        foreach (var audioSource in parent.GetComponentsInChildren<UISceneAudio>())
                        {
                            audioSource.Stop();
                        }
                    }
                }

                if (videoAsset.DuckingSettings.DuckBackgroundAudio)
                {
                    var sceneTransitionEvent = GetComponentInParent<ISceneMessage<AudioPlayerDuckEvent>>();
                    if (sceneTransitionEvent != null)
                    {
                        sceneTransitionEvent.HandleSceneEvent(new AudioPlayerDuckEvent(true, videoAsset.DuckingSettings.DuckAmountInDb));
                    }
                }
            }
        }

        public override void UpdateSceneTransform()
        {
            base.UpdateSceneTransform();

            RotateTransformToFaceCamera(transform, m_faceCameraHorizontally, m_faceCameraVertically);
        }
    }
}