﻿using System;

namespace Kingfisher.Permissions
{
    public interface IVRPermissionRequest
    {
        bool HasPermissions();
        void RequestPermissions(Action<bool> callback);
    }
}