﻿using System;

namespace Kingfisher.Permissions
{
    public class DummyPermissionRequester : IVRPermissionRequest
    {
        public bool HasPermissions()
        {
            return true;
        }

        public void RequestPermissions(Action<bool> callback)
        {
            if (callback != null)
                callback.Invoke(true);
        }
    }
}
