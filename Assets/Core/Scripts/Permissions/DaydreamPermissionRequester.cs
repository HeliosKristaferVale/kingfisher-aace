﻿using System;
using UnityEngine;

namespace Kingfisher.Permissions
{
    public class DaydreamPermissionRequester : IVRPermissionRequest
    {
#pragma warning disable 0414 // Assigned but never used emits if editor not set to Android/Daydream.
        private static string[] m_requirePermissionsNames = { "android.permission.READ_EXTERNAL_STORAGE" };
#pragma warning restore 0414

        public bool HasPermissions()
        {
#if UNITY_HAS_GOOGLEVR && UNITY_ANDROID
            GvrPermissionsRequester permissionRequester = GvrPermissionsRequester.Instance;
            if (permissionRequester == null)
            {
                Debug.Log("GvrPermissionsRequester instance not found. Falsifying permissions to true.");
                return true;
            }

            foreach (var permission in m_requirePermissionsNames)
            {
                bool granted = permissionRequester.IsPermissionGranted(permission);
                if (!granted)
                    return false;
            }

            return true;
#else
            return true;
#endif  // UNITY_HAS_GOOGLEVR && UNITY_ANDROID
        }

        public void RequestPermissions(Action<bool> callback)
        {
#if UNITY_HAS_GOOGLEVR && UNITY_ANDROID
            GvrPermissionsRequester permissionRequester = GvrPermissionsRequester.Instance;
            if (permissionRequester == null)
            {
                Debug.LogWarning("IVRPermissionRequest RequestPermission failed due to no GvrPermissionRequester instance!");
                if(callback != null)
                    callback.Invoke(false);
                return;
            }

            permissionRequester.RequestPermissions(m_requirePermissionsNames, results =>
            {
                bool hasAllPermissions = true;
                foreach (var result in results)
                {
                    Debug.LogFormat("IVRPermissionRequest Requested permission: {0} granted: {1}", result.Name, result.Granted);
                    if (!result.Granted)
                    {
                        Debug.LogWarningFormat("IVRPermissionRequest RequestPermission was denied permission to Permission {0}, application may fail to run.", result.Name);
                        hasAllPermissions = false;
                    }
                }

                if (callback != null)
                    callback.Invoke(hasAllPermissions);
            });
#else
            if(callback != null)
                    callback.Invoke(true);
#endif  // UNITY_HAS_GOOGLEVR && UNITY_ANDROID
        }
    }
}
