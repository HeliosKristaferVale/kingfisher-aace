﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

namespace Kingfisher
{
    [RequireComponent(typeof(MeshRenderer))]
    public class VideoTransitionMesh : MonoBehaviour
    {
        public const float kTransitionSpeed = 0.75f;
        public float TransitionDurationSeconds { get { return 1f / kTransitionSpeed; } }

        private Material m_materialInstance;
        private MeshRenderer m_meshRenderer;

        private void Awake()
        {
            EnsureRenderingInstancesExist();
        }

        private void OnDestroy()
        {
            Destroy(m_materialInstance);
            m_materialInstance = null;
        }

        public void FadeIn(bool skipTransition = false)
        {
            StartCoroutine(DoTransitionFadeInCoroutine(skipTransition));
        }

        public void FadeOut(bool skipTransition = false)
        {
            StartCoroutine(DoTransitionFadeOutCoroutine(skipTransition));
        }

        public virtual void SetTransitionColor(Color newColor)
        {
            EnsureRenderingInstancesExist();
            m_materialInstance.SetColor("_Color", newColor);
        }

        public IEnumerator DoTransitionFadeInCoroutine(bool skipTransition = false)
        {
            EnsureRenderingInstancesExist();
            SetMeshRendererVisible(true);

            if (skipTransition)
            {
                SetMaterialAlpha(1f);
                yield break;
            }

            m_materialInstance.DOKill();
            yield return m_materialInstance.DOFade(1f, kTransitionSpeed).SetEase(Ease.Linear).SetSpeedBased().WaitForCompletion();
        }

        public IEnumerator DoTransitionFadeOutCoroutine(bool skipTransition = false)
        {
            EnsureRenderingInstancesExist();

            if (skipTransition)
            {
                SetMaterialAlpha(0f);
                SetMeshRendererVisible(false);

                yield break;
            }

            m_materialInstance.DOKill();
            yield return m_materialInstance.DOFade(0f, kTransitionSpeed).SetEase(Ease.Linear).SetSpeedBased().OnComplete(() =>
            {
                SetMeshRendererVisible(false);
            }).WaitForCompletion();
        }

        protected virtual void SetMaterialAlpha(float matAlpha)
        {

            Color existColor = m_materialInstance.GetColor("_Color");
            existColor.a = matAlpha;
            m_materialInstance.SetColor("_Color", existColor);
        }

        /// <summary>
        /// Snippet function so we can litter it everywhere. This lets us smooth out any execution order
        /// issues.
        /// </summary>
        protected virtual void EnsureRenderingInstancesExist()
        {
            if (m_meshRenderer == null)
                m_meshRenderer = GetComponent<MeshRenderer>();

            if (m_materialInstance == null)
                m_materialInstance = m_meshRenderer.material;
        }

        /// <summary>
        /// Snipper function to change the visiblity of the MeshRenderer. We do this so we can turn off the
        /// mesh when it's not doing a transition to save a little bit on early rejects in the shaders. 
        /// Mobile and all that.
        /// </summary>
        /// <param name="visible"></param>
        protected virtual void SetMeshRendererVisible(bool visible)
        {
            m_meshRenderer.enabled = visible;
        }
    }
}
