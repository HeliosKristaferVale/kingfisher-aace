﻿using Kingfisher.ScriptableObjects.Scenes;
using Kingfisher.UI;
using System.Collections;
using UnityEngine;

namespace Kingfisher
{
    public class BaseStateMachineState : MonoBehaviour, IStateMachineReciever
    {
        public string SceneName { get { return m_baseSceneAsset.SceneName; } }

        protected KingfisherApplication m_application;
        protected BaseSceneAsset m_baseSceneAsset;

        public virtual IEnumerator OnStateEntered()
        {
            if (m_baseSceneAsset.RecenterViewOnEnter)
            {
                m_application.RecenterView();
            }
            yield break;
        }

        public virtual IEnumerator OnStateExited()
        {
            // We can't do this when we enter the state because then anything inside the scene who wants to do
            // first-time-visit stuff already thinks it's visited. This way we only say we've visited when we leave.
            m_application.MarkSceneAsVisited(SceneName);
            yield break;
        }

        public virtual void LoadFromAsset(BaseSceneAsset scene)
        {
            gameObject.name = scene.name + "_Scene";

            m_baseSceneAsset = scene;

            foreach (var assetData in scene.SceneAssets)
            {
                if(assetData == null)
                {
                    Debug.LogWarningFormat("null asset data in Scene's SceneAssets. Scene: {0}", scene.name);
                    continue;
                }

                // Intantiate a copy of the prefab speified by this asset
                UIBasePrefab prefab = assetData.Prefab;
                if (prefab != null)
                {
                    UIBasePrefab newPrefab = Instantiate(prefab, transform);
                    newPrefab.LoadFromAsset(assetData);
                }
            }
        }

        public void SetStateMachine(KingfisherApplication machine)
        {
            m_application = machine;
        }

        public virtual void Tick(float deltaTime) { }
    }
}