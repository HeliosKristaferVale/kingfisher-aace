﻿using UnityEngine;

namespace Kingfisher
{
    public class EditorCameraControls : MonoBehaviour
    {
        [SerializeField] private float m_sensitivityX = 1000;
        [SerializeField] private float m_sensitivityY = 1000;
        [SerializeField] private float m_minimumY = -85;
        [SerializeField] private float m_maximumY = 85;

        private float m_rotationY;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            if (!Application.isEditor)
                return;

            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * m_sensitivityX * Time.deltaTime;

            m_rotationY += Input.GetAxis("Mouse Y") * m_sensitivityY * Time.deltaTime;
            m_rotationY = Mathf.Clamp(m_rotationY, m_minimumY, m_maximumY);

            transform.localEulerAngles = new Vector3(-m_rotationY, rotationX, 0);
        }
    }
}