﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace Kingfisher
{
    public class KingfisherUnityVideoPlayerMediaPlayer : KingfisherMediaPlayer
    {
        private VideoPlayer m_mediaPlayerComponent;   // This is the unity video player.
        private bool shouldLoop = false;

        private void Setup()
        {
            m_mediaPlayerComponent = GetComponent<VideoPlayer>() == null ? gameObject.AddComponent<VideoPlayer>() : GetComponent<VideoPlayer>();
            //m_mediaPlayerComponent.playOnAwake = true;
            m_mediaPlayerComponent.loopPointReached += OnLoopPointReached;
            m_mediaPlayerComponent.prepareCompleted += OnVideoPrepared;
            //m_mediaPlayerComponent.isLooping = shouldLoop;
        }

        // Both
        private void Update()
        {
            //if (m_mediaPlayerComponent != null)
            //{
            //    //m_isPlaying = m_mediaPlayerComponent.isPlaying;
            //}

            //if (!m_isPlaying)
            //{
            //    Debug.LogFormat("KingfisherUnityVideoPlayer::Update::m_isPlaying::False");
            //    return;
            //}

            if (m_mediaPlayerComponent != null && m_mediaIsLoaded)
            {
                CurrentTimeInSeconds = (float)m_mediaPlayerComponent.time;
            }
        }

        // Unity Video Player
        private void OnLoopPointReached(VideoPlayer source)
        {
            m_mediaIsLoaded = false;
            DurationInSeconds = 0f;
            CurrentTimeInSeconds = 0f;

            if (OnVideoPlaybackFinished != null)
                OnVideoPlaybackFinished.Invoke();
        }

        private void OnVideoPrepared(VideoPlayer source)
        {
            if (m_onFinishedLoadingCallback != null)
                m_onFinishedLoadingCallback.Invoke();
        }

        // Both
        public override void LoadMediaAsync(string mediaName, Action onFinishedLoading)
        {
            Setup();
            m_isPlaying = false;
            m_mediaIsLoaded = false;
            CurrentTimeInSeconds = 0f;
            DurationInSeconds = 0f;
            m_onFinishedLoadingCallback = onFinishedLoading;

            // This will automatically close any existing file that is playing.
            string filePath = m_application.DoPathFixup(mediaName);

            m_mediaPlayerComponent.url = filePath;
            m_mediaPlayerComponent.Prepare(); // This is optional but good to do.
            m_mediaPlayerComponent.renderMode = VideoRenderMode.MaterialOverride;
            m_mediaPlayerComponent.targetMaterialRenderer = m_application.MediaSphere.GetComponent<MeshRenderer>();

            int parsedFrameCount;

            if (Int32.TryParse(m_mediaPlayerComponent.frameCount.ToString(), out parsedFrameCount))
            {
                if (parsedFrameCount <= 0)
                {
                    Debug.LogErrorFormat("{0}::KingfisherUnityVideoPlayer::LoadMediaAsync::parsedFrameCount <= 0. parsedFrameCound::{1}, frameCountAsString::{2}", GetDate(), parsedFrameCount, m_mediaPlayerComponent.frameCount.ToString());
                }
                else
                {
                    Debug.LogErrorFormat("{0}::KingfisherUnityVideoPlayer::LoadMediaAsync::parsedFrameCount > 0!", GetDate(), parsedFrameCount);
                }
            }
            else
            {
                Debug.LogErrorFormat("{0}::KingfisherUnityVideoPlayer::LoadMediaAsync::parsedInt is null!", GetDate());
            }

            DurationInSeconds = m_mediaPlayerComponent.frameCount / m_mediaPlayerComponent.frameRate;

            if (DurationInSeconds >= 0)
            {
                m_mediaIsLoaded = true;
            }
            else
            {
                Debug.LogErrorFormat("{0} ::KingfisherUnityVideoPlayer::LoadMediaAsync::\nVideo Duration in Seconds <= 0. DurationInSeconds::{1}", GetDate(), DurationInSeconds);
                Debug.LogErrorFormat("m_mediaPlayerComponent.frameCount::{0}", m_mediaPlayerComponent.frameCount.ToString());
            }
        }

        string GetDate()
        {
            return System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
        }

        public override void UnloadMedia()
        {
            m_mediaPlayerComponent.Stop();
            m_mediaIsLoaded = false;
            DurationInSeconds = 0f;
            CurrentTimeInSeconds = 0f;
        }

        public override void SetLoops(bool loops)
        {
            shouldLoop = loops;
            if (m_mediaPlayerComponent != null)
            {
                m_mediaPlayerComponent.isLooping = shouldLoop;
            }
        }

        public override void Play()
        {
            if (!m_mediaIsLoaded)
            {
                Debug.LogWarning("Media not loaded yet, ignoring call to play.");
                return;
            }

            m_isPlaying = true;
            m_mediaPlayerComponent.Play();
        }

        public override void Pause()
        {
            if (!m_mediaIsLoaded)
            {
                Debug.LogWarning("Media not loaded yet, ignoring call to pause.");
                return;
            }

            m_mediaPlayerComponent.Pause();
        }

        public override void Stop()
        {
            DurationInSeconds = 0f;
            CurrentTimeInSeconds = 0f;
            m_isPlaying = false;
            if (m_mediaPlayerComponent != null)
                m_mediaPlayerComponent.Stop();
        }
    }
}
