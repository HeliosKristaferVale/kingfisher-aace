﻿using Kingfisher.ScriptableObjects;
using UnityEngine;

namespace Kingfisher
{
    public class Reticle : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform m_reticle;

        private Material m_materialCache;

        private void OnDestroy()
        {
            Destroy(m_materialCache);
            m_materialCache = null;
        }

        public virtual void LoadFromAsset(ReticleAsset fromObject)
        {
            if (m_materialCache == null)
                m_materialCache = m_reticle.GetComponent<MeshRenderer>().material;

            SetReticleTexture(fromObject.Sprite);
            m_reticle.localPosition = new Vector3(0, 0, fromObject.Distance);
            m_reticle.localScale = Vector3.one * fromObject.Scale;
        }

        public void SetReticleTexture(Texture sprite)
        {
            m_materialCache.SetTexture("_MainTex", sprite);
        }
    }
}