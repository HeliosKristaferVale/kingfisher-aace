﻿using System;
using UnityEngine;
using System.Collections;


namespace Kingfisher
{
    public abstract class KingfisherMediaPlayer : MonoBehaviour, IStateMachineReciever, IKingfisherMediaPlayer
    {
        public Action OnVideoPlaybackFinished;                     // Both
        public Action<Texture, bool> OnPlaybackTextureChanged;     // AVPro what is the UnityVideoPlayer equivalent?
        public float DurationInSeconds { get; internal set; }      // Both
        public float CurrentTimeInSeconds { get; internal set; }   // Both
        public bool IsPlaying { get { return m_isPlaying; } }      // Both

        protected bool m_isPlaying;                                // Both
        protected bool m_mediaIsLoaded;                            // Both
        protected Action m_onFinishedLoadingCallback;              // Both
        protected KingfisherApplication m_application;             // Both

        public void SetStateMachine(KingfisherApplication app)
        {
            m_application = app;
        }

        public abstract void SetLoops(bool loops);
        public abstract void Play();
        public abstract void Stop();
        public abstract void Pause();
        public abstract void UnloadMedia();
        public abstract void LoadMediaAsync(string mediaName, Action onFinishedLoading);
    }
}
