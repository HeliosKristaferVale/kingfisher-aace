﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kingfisher;
using UnityEngine;

namespace Kingfisher
{
    public interface IKingfisherMediaPlayer
    {
        void Play();
        void Stop();
        void Pause();
        void UnloadMedia();
        void SetLoops(bool loops);
        void LoadMediaAsync(string mediaName, Action onFinishedLoading);
    }
}