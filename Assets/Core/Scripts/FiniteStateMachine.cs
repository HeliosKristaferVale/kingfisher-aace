﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kingfisher
{
    public class FiniteStateMachine : MonoBehaviour
    {
        private List<BaseStateMachineState> m_stateList;

        private bool m_isInStateTransition = false;
        protected BaseStateMachineState m_currentState;

        protected virtual void Awake()
        {
            m_stateList = new List<BaseStateMachineState>(GetComponentsInChildren<BaseStateMachineState>(true));

            // We're going to disable each child so that it 'cleans' the state of the app so that it doesn't matter
            // where you are when you build/start play mode.
            foreach (var state in m_stateList)
            {
                state.gameObject.SetActive(false);
            }
        }

        protected virtual void Update()
        {
            if (m_isInStateTransition || m_currentState == null)
                return;

            m_currentState.Tick(Time.deltaTime);
        }

        public void ChangeState(string stateName)
        {
            StartCoroutine(ChangeStateInternal(stateName));
        }

        private IEnumerator ChangeStateInternal(string stateName)
        {
            if (m_isInStateTransition)
            {
                Debug.LogWarningFormat("Attempted to change state to \"{0}\" while already in a state transition, ignoring...", stateName);
                yield break;
            }

            m_isInStateTransition = true;

            // If we're already in a state, transition out of it.
            if (m_currentState != null)
            {
                yield return StartCoroutine(m_currentState.OnStateExited());
                m_currentState.gameObject.SetActive(false);
                m_currentState = null;
            }

            // Find our new state and transition into it.
            BaseStateMachineState nextState = null;
            foreach (var state in m_stateList)
            {
                if (state.SceneName == stateName)
                {
                    nextState = state;
                    break;
                }
            }

            if (nextState != null)
            {
                m_currentState = nextState;
                m_currentState.gameObject.SetActive(true);
                yield return StartCoroutine(nextState.OnStateEntered());
            }
            else
            {
                Debug.LogErrorFormat("Unknown State: \"{0}\" Not Transitioning!", stateName);
            }

            m_isInStateTransition = false;
        }
    }
}