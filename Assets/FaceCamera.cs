﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour {

	void Start()
	{
		RotateTransformToFaceCamera(transform, true, true);
	}
	protected virtual void RotateTransformToFaceCamera(Transform inTransform, bool bRotateHorizontal, bool bRotateVertical)
        {
            Vector3 dirToCamera = inTransform.position;

            // If they don't want it to rotate horizontally to face the camera, skip that
            if (!bRotateHorizontal)
            {
                dirToCamera.x = 0;
                dirToCamera.y = 0;
            }

            // If they don't want to rotate vertically to face the camera, skip that too
            if (!bRotateVertical)
            {
                dirToCamera.y = 0;
            }

            dirToCamera.Normalize();
            inTransform.rotation = Quaternion.LookRotation(dirToCamera, Vector3.up);
        }


}
