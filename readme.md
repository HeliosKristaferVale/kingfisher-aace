# Kingfisher

Kingfisher is a framework built to make routine 360 video playback cross platform, with a focus on being easy to build, extend and test. To satisfy these requirements, Kingfisher is built around Unity's `ScriptableObject`s which **allows the entire project to be defined as a series of assets with no changes to the Unity scene required**. As an additional benefit, breaking the project up into multiple `ScriptableObject`s and not making changes in the main Unity scene allows multiple developers to work on it without worrying about merging complex Unity scenes/prefabs.

Kingfisher is cross platform (Daydream, GearVR, Google Cardboard) and tries to maintain equivelant functionality across all platforms. To make all of these goals possible, **Kingfisher is provided as a framework you build ontop of, not as a component you add to an existing project.** - If you want to try integrating Kingfisher into an existing codebase, that's on you!

## Requirements

### Unity 5.6+

[Unity 5.6+](https://unity3d.com/unity/beta) This is currently built against Unity 5.6.0b8 Beta, but should work on future versions as long as Daydream is supported.

### JDK 8

If building and deploying to the phone fails with "Failed to compile resources with the following parameters:" error, update to the latest version of the Java Development Kit (JDK8). Then ensure you have pointed your Unity Preferences to the new path of JDK8 as it will by default continue to point to the old version.

## uGUI Support

Kingfisher tries to support standard uGUI components as much as possible. *Enter*, *Exit*, *Down*, *Up*, *Click*, *Select*, *Deselect*, *UpdateSelected*, and *VRPointerHover* events are supported through standard uGUI interfaces. *Scroll*, *Move*, and *Submit*/*Cancel* events are not emitted. Some events may only be supported in Daydream which supports clicking.

To make a Canvas work with Kingfisher's uGUI support you must **remove the standard `GraphicRaycaster` script, and add instead add a `VRPointerGraphicRaycaster` script**. This is required so that uGUI elements recieve the events from Kingfisher's uGUI support, and your uGUI will be unreponsive without it.

## VR Home (Daydream)

Google Daydream supports an in-VR app browser. For the Daydream application to show up in VR Home, you must provide two 512x512 assets - a foreground and a background. VR Home will automatically create a parallax effect between the two, so you can use the foreground for a logo and the background as an image from the application. To set these in Unity, set them inside of Player Settings > Virtual Reality SDKs > Daydream.

## Performance Tweaks
If importing Kingfisher into an existing Unity Project, consider changing the following settings for performance and visual improvements. If cloning the Kingfisher repo entirely these settings should already be applied.

* Enable 2x Anti Aliasing. (Unity will issue a warning about performance when you build, but it performs fine and looks bad without it.)
* Disable real time shadows.
* Disable "Use 32-bit Display Buffer" (Watch for color banding on content)
* Use a 16-bit Depth Buffer (Watch for z-fighting in scenes)
* Use Forward or Vertex Rendering
* Enable Multithreaded Rendering
* Enable Static and Dynamic batching
* Disable VSync - the VR SDK's roll their own sync technology anyways.


# Using Kingfisher

## Using Existing Kingfisher Functionality

Kingfisher comes with a fair amount of built-in functionality, enough to build a basic 360 VR experience. If Kingfisher does not already contain the expected functionality it can be expanded, though there is some complexity in doing so (see below). 


### VR Tour
To build a basic experience you will need to create a VR Tour (`Create > Kingfisher > VR Tour`) via right clicking in the *Project* window. The `VR Tour` represents the root level object of the tour and contains references to everything needed by the tour. The most important fields are documented below:

* Default Scene - Name of the Scene to first load after loading the application.
* SD Card Root Folder - Name of the folder on the SD Card that videos go in. Used to do an automatic check on boot to ensure sdcard content was installed correctly.
* Scene List - An array of `BaseSceneAsset` which define the various Scenes within the application. Kingfisher can only transition between scenes specified in this list so all related scenes must be specified here.
* Transition Color - What color is the transition overlay when it fades in. Can be used to match brand colors.

### Scenes

Kingfisher comes with several pre-made scene types. The various scene types can be found under `Create > Kingfisher > Scenes`. 

**Simple Video Playback Scene** is a standard out of the box 360 video playback scene. It begins playing the video as soon as the scene loads, optionally loops, and can optionally transition to the next scene when the video is finished playing. **Scene Name** is the name of this scene that is used for Kingfisher transitioning. **Media Path** is relative to the root folder specified by the `VR Tour`, and **Next Scene Name** is the **Scene Name** of another `BaseSceneAsset` to transition to. Under current Kingfisher design only one video can be playing at a time which is why the entire scene is dedicated to them. **Auto Transition** specifies if the scene should automatically transition to the specified scene when the video loads. This setting is ignored if **Loops** is set to true, which will cause the video to play in a loop.

**Simple Image Playback Scene** is a standard out of the box 360 image playback scene. It places the specified Image onto the 360 sphere when the scene loads. This is similar in purpose to the Simple Video Playback Scene, but is used to save designers from having to create non-moving videos out of still frames they wish to use. **Auto Transition** will automatically switch to the **Next Scene Name** scene after the specified **Auto Transistion Scene Length**.

## Extending Kingfisher
Kingfisher is built around the concept of `Scenes`. Kingfisher automatically transitions between scenes and can hide the transition by fading the `TransitionSphere` in and out. There are 